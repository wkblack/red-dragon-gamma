# Red Dragon Gamma

Red sequence selection via Gaussian Mixture Modeling

For assistance running, please contact 
[William K. Black](mailto:wkblack@umich.edu) at 
[wkblack@umich.edu](mailto:wkblack@umich.edu)


## Dependencies

Red Dragon requires tools from `scipy`, `sklearn`, `pygmmis`, 
and `h5py` (among other, more common packages). 

## Usage

In this example, a three-Gaussian model of the RS is created.
```python
import rd_gamma

# create dragon DNA from source file 'data.h5'
rd_gamma.fit_file(fname_in='data.h5',N_fit=3,fname_out='out.h5')

# create dragon object from DNA output file
rd = rd_gamma.dragon('out.h5')

# measure red sequence membership probabilities from data
P_red = rd.P_red(Z,bands,bands_err)
```

The input dataset must be an HDF5 file with keys `Z` (shape **N**) 
and `bands` (shape **N, N_bands**). 
Error fields `Z_err` and `bands_err` (same shapes as `Z` and `bands`) 
are optional but recommended.


## How to train your dragon

Getting the Gaussian Mixture Model to match unambiguously across 
redshift can be challenging, especially with more than two components. 
After generating the rd DNA, begin matching by plotting the fits to each parameter.
```python
rd.plot_fits() # plot weights, means, and covariances
rd.plot_scattcorr() # plot scatters and correlations separately
```
Generally, the plot of mean color tends to best distinguish whether 
the components match well across redshift, with the plot of 
component weights giving additional useful information. 

For all sub-sections that follow, the general formula to follow is:
```python
rd = rd_gamma.dragon('out.h5') # load in dragon
... # make changes to matching, interpolation, etc.
rd.set_fits() # update interpolation lines
rd.plot_fits() # check that changes look good
rd.save_training() # overwrite out.h5 with new fitting
```


### Component matching

At individual redshifts, if components are mismatched, then 
use the `sort` method to swap indices. For example, if you 
have a three-component model, and components two and three 
were mismatched at redshift `Z=0.2`, then you can swap them by running
```python
idx = rd.idx_where_Z(0.2) # find the index for redshift 0.2
rd.sort(idx,[0,2,1]) # swap association of second and third components
rd.set_fits() # recalibrate fits
```

It can also be useful to plot aggregate values across colors and covariances, 
giving the net scatter relative to total scatter of all colors, and 
giving the color relative to mean net color in terms of net scatter. 
```python
rd.plot_WCR() # plot weight, color sigmas from mean, & relative scatter
```
This 'WCR' method combines across colors, giving a quick
and simplified view of continuity and component matching.


Ultimately, fits must be checked by the user, corrected by hand. 
However, a promising method for checking component continuity is: 
```python
rd.sort_from_peak() # sort parameters by nearest neighbors
rd.set_fits() # recalibrate fits
```
which looks for distinct regions between parameters, then sorts from there. 


### Interpolation settings

Interpolation between fit parameters w, mu, Sigma are linear by default,
with flat endpoint extrapolation. Spline and KLLR fitting are also available.
To turn on these methods, one must signify which method to use, then 
in the case of spline and KLLR, choose a smoothign kernel. 
```python
# linear
rd.method_w = 'interp1d' # default
# spline
rd.method_mu = 'spline' # use spline interpolation
rd.softening_w_spline = .0075 # softening scale for spline
# KLLR
rd.method_Sigma = "KLLR" # use kernel-localized smoothing (recommended)
rd.KLLR_kw = .025 # Gaussian width in smoothing
```
KLLR gives excellent fits, with its Gaussian kernel sliding across
redshift to smooth out fit parameters. Generally, you'll want a kernel
width >= your redshift bin width; the default is double the bin width.
Spline fits tend to be more finicky, but generally the softening factors 
should be around 0.001 to 0.01 (though datasets respond differently).


## Future work: Red Dragon Gamma

If you would like to fork the Red Dragon code or continue development, 
please contact [William K. Black](mailto:wkblack@umich.edu) at 
[wkblack@umich.edu](mailto:wkblack@umich.edu), as there are several
important improvements to the code that could be made. 

The next version of the Red Dragon algorithm should see several updates: 

 * Paralellize the code, such that multiple redshift bins can be 
    analyzed at once by pyGMMis. Save individual bin files in parallel 
    (which will later be ammassed into a `dragon` object).  
 * Make the code pip-installable. 
 * Allow for variable bin width. Even better: rather than fit in bins, 
    fit using some kernel (e.g. selecting galaxies using Gaussian 
    kernel, probabilistically selecting them). Then rather than having 
    a hardcoded secondary variable, secondary traits can be summarized
    for a given pyGMMis fit, allowing for interpolation across many
    different parameters, rather than a single one (as now written). 
 * Remove all fit options besides linear and KLLR.
 * Allow for non-zero correlations between band noise. This is needed
    for drift-scan surveys like SDSS, where atmospheric effects can
    have high correlation on their effects between bands. 



## Citation
Please cite [Black & Evrard 2022](https://doi.org/10.1093/mnras/stac2052) if you make use of this code. 

See also [Black & Evrard 2023](https://doi.org/10.48550/arXiv.2310.09374) for updated algorithm considerations, such as suggested component count and bin resolution for redshift or stellar mass. 


