# goal: test how bootstrapping results goes
#       e.g. how resilient are w, mu, Sigma to resampling?

import rd_gamma; np,plt,h5py = rd_gamma.np,rd_gamma.plt,rd_gamma.h5py # improt
from mpl_tools import * # import matplotlib tools and defaults

from scipy.interpolate import interp1d

from m_star_model import m_star

from scipy.special import erf
s1,s2,s3,s4,s5 = [erf(ii/np.sqrt(2)) for ii in range(1,6)]

# suppress warnings
import warnings
warnings.filterwarnings("ignore",category=RuntimeWarning)


# add KLLR (new version)
import sys
path_kllr = '/global/u1/w/wkblack/kllr/'
sys.path.append(path_kllr)
from kllr import kllr_model
kw = .03
lm = kllr_model(kernel_width=kw) # cf .02 and .05
T = np.transpose # fix messed up KLLR outputs

if 0: # test KLLR
  x,y = np.random.random((2,100))
  lm = kllr_model()
  out = lm.fit(x,y,kernel_width=.1,y_err=.1*np.ones(100),nBootstrap=1)
  xline, yline, intercept, slopes, scatter, skew, kurt = out
  plt.scatter(x,y)
  plt.plot(xline,T(yline))
  plt.fill_between(xline,T(yline-scatter),T(yline+scatter),alpha=.125,lw=0)
  plt.show()
  print(np.shape(out))
  assert(1==0)

# add redMaPPer comparison
from RM_cf import RM_fit


########################################################################
# 1) Run RD on severe sub-samples of the data
# fdir = 'output/bootstrap_test_01/' # initial Z|(0,1) test
# fdir = 'output/bootstrap_test_02/' # 10^4 sample
# fdir = 'output/bootstrap_test_03/' # 10^5 sampling
# fdir = 'output/bootstrap_test_04/' # 10^6 sampling
# fdir = 'output/bootstrap_test_05/' # 10^5 sampling
fdir = 'output/bootstrap_test_06/' # 10^6 sampling


N_bootstrap = 100
idx_start = 0 # index to start next few runs on; start w/ zero

def run_bootstrap(N_max=10**4):
  # dataset for analysis
  f_in = rd_gamma.fname_Bz_R14 
  f_init = "output/fit_Bz_R14_2K_coarse.h5"
  # f_init = "output/bootstrap_test_04/fit_Bz_R14_2K_001.h5" # KLLR-fit higher run
  
  print("Running boostrap...")
  for ii in range(idx_start,N_bootstrap):
    f_out = fdir + f"fit_Bz_R14_2K_{ii:03}.h5"
    print(f"\nrun {ii+1-idx_start}/{N_bootstrap-idx_start}; creating {f_out}")
    rd_gamma.fit_file(f_in,2,fname_out=f_out,N_max=N_max,
                      dZ_1=.025,use_initial=f_init,Z_min=.05,Z_max=.84)
  
  print("~fin")


if (__name__ == '__main__') and False:
  # run_bootstrap(N_max=10**6) 
  run_bootstrap(10**6)



########################################################################
# 2) Collect w, mu, Sigma fit parameters from those runs; calculate means, scatters

# gather hyperparameters
fnames = [fdir + f"fit_Bz_R14_2K_{ii:03}.h5" for ii in range(N_bootstrap)]
with h5py.File(fnames[0],'r') as ds:
  Z_bins = ds['Z_bins'][:]
  Z_mids = (Z_bins[:-1]+Z_bins[1:])/2.
  NZ,K,D = ds['mu'].shape
  N_pars = K * (D**2 + D + 1) - 1
  print("Number of parameters:",N_pars) 

# gather GMM parameters
w_all = np.zeros((len(fnames),NZ,K))
mu_all = np.zeros((len(fnames),NZ,K,D))
Sigma_all = np.zeros((len(fnames),NZ,K,D,D))

for ii in range(len(fnames)):
  with h5py.File(fnames[ii],'r') as ds:
    w_all[ii] = ds['w'][:]
    mu_all[ii] = ds['mu'][:]
    Sigma_all[ii] = ds['Sigma'][:]


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up weights, means, and covariance elements 
meansig = lambda x: [f(x,axis=0) for f in [np.nanmean,np.nanstd]]
S = s2 # which standard deviation to use
qtls = [.5-S/2., .5, .5+S/2.]
print("Using quantiles:",np.round(qtls,4))

# weights 
w_mean,w_sig = meansig(w_all)
w_quants = np.quantile(w_all,qtls,axis=0)
w_errs = np.diff(w_quants,axis=0)
# means
mu_mean,mu_sig = meansig(mu_all)
mu_quants = np.quantile(mu_all,qtls,axis=0)
mu_errs = np.diff(mu_quants,axis=0)
# covariance terms 
Sigma_mean,Sigma_sig = meansig(Sigma_all)

scats = np.sqrt(np.diagonal(Sigma_all,0,-1,-2)) # calculate scatters
corr = rd_gamma._corr_from_Sigma(Sigma_all) # calculate correlations

scats_quants = np.quantile(scats,qtls,axis=0)
scats_errs = np.diff(scats_quants,axis=0)
corr_quants = np.quantile(corr,qtls,axis=0)
corr_errs = np.diff(corr_quants,axis=0)


########################################################################
# plot fits 

fs = plt.rcParams['font.size'] # font size

def plot_KLLR(where,x,y,sig,col='k'):
  " plot mean and error on mean from KLLR "
  assert(np.all(np.array(sig)>0))
  pars = 1000,50 # high quality
  # pars = 100,50 # good
  # pars = 5,10 # test runs
  out = lm.fit(x,y,y_err=sig,nBootstrap=pars[0],bins=pars[1])
  xline, yline, intercept, slopes, scatter, skew, kurt = out
  mean,scat = [f(yline,axis=0) for f in [np.mean,np.std]]
  where.plot(xline,mean,color=col,lw=.25)
  where.fill_between(xline,mean-scat,mean+scat,lw=0,alpha=.125,color=col)


def plot_KLLR2(where,y,col='k',pars=[3,len(Z_mids)],log=False,Nsig=5):
  " plot mean and error on mean from KLLR "
  # choose N_bootstrap, N_bins (x)
  # pars = 100,50 # good
  # pars = 3,len(Z_mids) # test runs
  # pars = 1000,100 # high quality
  # set up x and y variables to repeat properly if multidimensional
  bool_multidim = np.shape(y)[0] != np.product(np.shape(y))
  if bool_multidim:
    x = np.repeat(Z_mids,N_bootstrap)
    y = np.reshape(np.transpose(y),np.product(np.shape(y)))
    assert(x.shape==y.shape)
  if log:
    y = np.log(y)
  out = lm.fit(x,y,nBootstrap=pars[0],bins=pars[1])
  xline, yline, intercept, slopes, scatter, skew, kurt = out
  mean,scat = [f(yline,axis=0) for f in [np.nanmean,np.nanstd]]
  lo,md,hi = vals = mean-Nsig*scat, mean, mean+Nsig*scat
  if log:
    lo,md,hi = np.exp(vals)
  where.plot(xline,md,color=col,lw=.25)
  where.fill_between(xline,lo,hi,lw=0,alpha=.125,color=col)


H09_w = {
  "source" : "https://arxiv.org/pdf/0907.4383.pdf", # figure 11a,
  # so not official. 
  "z_vals" : [.119460,.131541,.146488,.161502,.176509,.191490],
  "w_vals" : np.transpose([[.58,.41],[.56,.43],[.55,.44],
                           [.54,.45],[.52,.47],[.47,.52]]),
  "color" : "xkcd:bluey grey", 
  # "color" : "xkcd:salmon",
  # "color" : "xkcd:warm grey",
  "label" : "H09"
}


def plot_w(fit_KLLR=True,cf_Hao=False,break_lines=True,use_median=True,
           display=True,high_res=False):
  for ii in range(K):
    col = rd_gamma.mpl_colors_RF[ii]
    wm,ws = w_mean[:,ii], w_sig[:,ii] # mean and scatter 
    if use_median:
      wm = w_quants[1,:,ii] # median weight 
      we = w_errs[:,:,ii] # uneven errors for errorbar plot
      plt.errorbar(Z_mids,wm,we,capsize=2,fmt='o',color=col,ms=3)
    else: # use mean and std dev
      plt.errorbar(Z_mids,wm,ws,capsize=2,fmt='o',color=col,ms=3)
      # plt.fill_between(Z_mids,(wm-ws),(wm+ws),alpha=.25,lw=0,color=col)
    
    if fit_KLLR:
      # plot_KLLR(plt,Z_mids,wm,ws,col)
      
      if high_res:
        pars = [1000,100] # high qualtiy
      else:
        pars = [3,len(Z_mids)] # test runs
      
      plot_KLLR2(plt,w_all[:,:,ii],col,pars=pars)
  
  if cf_Hao and False: # deactivate; not official Hao+ fitting
    plt.scatter(H09_w["z_vals"],H09_w["w_vals"][0],
                color=H09_w["color"],label=H09_w["label"])
  
  if break_lines: # plot vertical lines for band transitions
    xlim = plt.xlim(plt.xlim()) # freeze x-axis
    ylim = plt.ylim(plt.ylim()) # freeze y-axis
    for z_L,z_R in zip(lines_4kA,lines_Balmer):
      col = 'xkcd:battleship grey'
      # draw 4000 Angstrom break redshift
      plt.vlines(z_L,*ylim,col,#'dashed',
                       lw=.5,alpha=.5,zorder=-1)
      if 0:
        # draw between 4000 and 3645 Angstrom for transition region
        o = np.ones(2)
        plt.fill_between([z_L,z_R],ylim[0]*o,ylim[1]*o,
                             alpha=.125,lw=0,color=col)
  
  plt.xticks(fontsize=fs)
  plt.yticks(fontsize=fs)
  plt.xlabel(r"Redshift")
  plt.ylabel(r"Weights")
  plt.savefig(fdir+'bootstrap_w.pdf')
  if display: plt.show()
  plt.clf()



label_colors = ['g-r','r-i','i-z']
if 1:
  # DES griz limits
  # lines_4kA = [.38,.78] # just the important ones there
  lines_4kA = [-.013,.381,.783,1.134,1.515]
  lines_Balmer = [.084,.516,.956,1.342,1.760]
else:
  # include also 3800 and 3645 Angstrom breaks
  lines_4kA = [.084,.040,-.013,.381,.454,.516,.783,.876,.956,
               1.134,1.246,1.342,1.515,1.647,1.760]


HaoEtAl2009 = { 
  # slope of [g-r vs i] relation vs redshift z
  "m_bR":3.049, "m_bR_s":.011,
  "b_bR":0.623, "b_bR_s":.002,
  # width of g-r ridgeline
  "m_sR":.136,  "m_sR_s":.010,
  "b_sR":.037,  "b_sR_s":.002,
  # g-r vs i slope
  "m_aR":-.075, "m_aR_s":.005,
  "b_aR":-.003, "b_aR_s":.001,
  # lower and upper redshift limits for interpolation
  "Z_range":[.1,.3], # used for extrapolation warnings
  'i_max_A':np.exp(3.1638), 'i_max_k':0.1428, # for m_{i,max}
  # "color" : "xkcd:bluey grey", 
  # "color" : "xkcd:salmon",
  "color" : "xkcd:warm grey",
  "label":"H09", # default label for plots 
  "url":"https://arxiv.org/pdf/0907.4383.pdf" # figs 7 and 9
}


def H09_mu(Z,delta_mag=0):
  """
  calculate g-r color from Hao+09 fitting.
  source: https://arxiv.org/pdf/0907.4383.pdf
  inputs
  ------
  Z : redshift
  delta_mag=0 : difference from R14 m_star
  """
  def i_max(Z):
    " see github.com/jgbrainstorm/pyjh/blob/master/pyhao/cmvlimi.py"
    A,k = [HaoEtAl2009[key] for key in ['i_max_A','i_max_k']]
    return A * Z**k
  
  def slope(Z):
    keys = ['m_aR', 'm_aR_s', 'b_aR', 'b_aR_s']
    m,m_err,b,b_err = [HaoEtAl2009[key] for key in keys]
    sl = m * Z + b
    sl_err = np.sqrt((m_err * Z)**2 + b_err**2)
    return sl,sl_err
  
  def zero_point(Z):
    keys = ['m_bR', 'm_bR_s', 'b_bR', 'b_bR_s']
    m,m_err,b,b_err = [HaoEtAl2009[key] for key in keys]
    zp = m * Z + b
    zp_err = np.sqrt((m_err * Z)**2 + b_err**2)
    return zp,zp_err
  
  # i-band magnitude at which to calculate mu relative to i_max
  delta_i = m_star(Z,warnings=False) + delta_mag - i_max(Z)
  sl,sl_err = slope(Z)
  zp,zp_err = zero_point(Z)
  gmr = sl * delta_i + zp
  gmr_err = np.sqrt((sl_err*delta_i)**2 + zp_err**2)
  return gmr,gmr_err


def H09_sigma(Z):
    keys = ['m_sR', 'm_sR_s', 'b_sR', 'b_sR_s']
    m,m_err,b,b_err = [HaoEtAl2009[key] for key in keys]
    lg_sig = m * Z + b
    lg_sig_err = np.sqrt((m_err * Z)**2 + b_err**2)
    return lg_sig, lg_sig_err


def plot_mu(fit_KLLR=True,cf_Hao=True,cf_RM=True,break_lines=True,
            use_median=True,display=True,high_res=False):
  " plot means "
  fig,axs = plt.subplots(1,D,sharex=True)
  axs[0].set_ylabel('Color')
  for ii in range(D):
    # set up labels
    ax = axs[ii]
    ax.set_xlabel("Redshift")
    lbl = r'$\langle ' + label_colors[ii] + r' \rangle$'
    ax.set_title(lbl)
    # plot points
    for jj in range(K):
      col = rd_gamma.mpl_colors_RF[jj]
      mm,ms = mu_mean[:,jj,ii], mu_sig[:,jj,ii]
      # errorbar plot
      if use_median:
        mm = mu_quants[1,:,jj,ii] # median mu
        me = mu_errs[:,:,jj,ii] # asymmetric errors
        ax.errorbar(Z_mids,mm,me,
                         capsize=2,fmt='o',color=col,ms=3)
      else:
        ax.errorbar(Z_mids,mm,ms,
                         capsize=2,fmt='o',color=col,ms=3)
      if 0: 
        # transparent plot
        ax.fill_between(Z_mids,(mm-ms),(mm+ms),
                             alpha=.25,lw=0,color=col)
      if fit_KLLR:
        # plot_KLLR(ax,Z_mids,mm,ms,col)
        
        if high_res:
          pars = [1000,100] # high qualtiy
        else:
          pars = [3,len(Z_mids)] # test runs
        
        plot_KLLR2(ax,mu_all[:,:,jj,ii],col,pars=pars)
      
    if break_lines:
      # plot vertical lines to show the 4kA break passing btw colors
      # freeze y-axis
      ylim = ax.get_ylim()
      ax.set_ylim(ylim)
      # freeze x-axis
      xlim = ax.get_xlim()
      ax.set_xlim(xlim)
      for z_L,z_R in zip(lines_4kA,lines_Balmer):
        col = 'xkcd:battleship grey'
        # draw 4000 Angstrom break redshift
        ax.vlines(z_L,*ylim,col,#'dashed',
                         lw=.5,alpha=.5,zorder=-1)
        if 0:
          # draw between 4000 and 3645 Angstrom for transition region
          o = np.ones(2)
          ax.fill_between([z_L,z_R],ylim[0]*o,ylim[1]*o,
                               alpha=.125,lw=0,color=col)
  
    ax.tick_params(axis='both', which='major', labelsize=fs)
  
  if cf_Hao:
    zv = np.linspace(.1,.3)
    col = HaoEtAl2009['color']
    lbl = HaoEtAl2009['label']
    if 0:
      for dmag,ls,lbl_ii in zip([+1.75,-0.00],[(0,(5,10)),'-'],[None,lbl]):
        gmr,gmr_err = H09_mu(zv,dmag)
        axs[0].plot(zv,gmr,linestyle=ls,color=col,label=lbl_ii,zorder=9,lw=1)
        axs[0].fill_between(zv,gmr-gmr_err,gmr+gmr_err,
                         alpha=.125,lw=0,color=col)
    else:
      gmr_lo,lo_err = H09_mu(zv,+1.75)
      gmr_hi,hi_err = H09_mu(zv,-0.00)
      axs[0].fill_between(zv,gmr_lo-lo_err,gmr_hi-hi_err,color=col,
                          label=lbl,zorder=-9,lw=0)
      pass
    axs[0].legend()
  
  if cf_RM:
    # set up general parameters
    dz = .05
    zv = np.linspace(.05-dz,.84+dz)
    col = RM_fit.color
    lbl = RM_fit.label
    # calculate mean color for various magnitudes
    for ii in range(D): # for each color
      if 0:
        for dmag,ls,lbl_ii in zip([+1.75,-0.00],[(0,(5,10)),'-'],[None,lbl]):
          c = RM_fit.get_mu(zv,ii,dmag)
          axs[ii].plot(zv,c,linestyle=ls,color=col,label=lbl_ii,zorder=9,lw=1)
      else:
        c_lo = RM_fit.get_mu(zv,ii,+1.75)
        c_hi = RM_fit.get_mu(zv,ii,-0.00)
        axs[ii].fill_between(zv,c_lo,c_hi,color=col,lw=0,alpha=.5,
                             zorder=-8,label=lbl)
    axs[0].legend(loc='lower right')
  
  plt.savefig(fdir+'bootstrap_mu.pdf')
  if display: plt.show()
  plt.clf()


def plot_scattcorr(fit_KLLR=True,cf_Hao=True,cf_RM=True,display=True,
                   break_lines=True,plot_err=True,use_median=True, high_res=False):
  " plot scatters and correlations "
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # scatters
  fig,axs = plt.subplots(1,D,sharex=True,sharey=True)
  sc = 10**np.mean(np.log10(scats),axis=0)
  mn,mx = np.nanquantile(sc,[0,1]) # min and max of means
  mn = max(mn/2.,10**np.floor(np.log10(mn)))
  mx = min(mx*2.,10**np.ceil(np.log10(mx)))
  mx = .3 # HARDCODE (for now)
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
  if plot_err: # import Buzzard errors
    fname_errs = 'output/bzz_err.h5' # output filename to save errors to
    with h5py.File(fname_errs) as ds:
      keys = ['Z_bins','Z_mids','Z_meds','quantiles']
      Z_bins_e,Z_mids_e,Z_meds_e,quantiles = [ds[k][:] for k in keys]
      N_bins,N_quant,_ = np.shape(quantiles)
      med = N_quant//2 # for median index
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
  # plot for each color
  for ii in range(D):
    # set up axes
    ax = axs[ii]
    if ii==0: # leftmost plot
      ax.set_ylabel('Scatter')
      ax.set_ylim(mn,mx)
    ax.set_xlabel('Redshift') # on all plots
    ax.set_yscale('log')
    ax.set_title(r'$\sigma_{%s}$' % label_colors[ii],fontsize=fs)
    # plot points
    for jj in range(K):
      col = rd_gamma.mpl_colors_RF[jj]
      lgsm,lgss = meansig(np.log10(scats))
      s_mean = 10**lgsm
      s_min,s_max = s_mean/10**lgss,s_mean*10**lgss
      err = np.array([s_mean-s_min, s_max-s_mean])
      if use_median:
        sm = scats_quants[1,:,jj,ii] # median mu
        se = scats_errs[:,:,jj,ii] # asymmetric errors
        ax.errorbar(Z_mids,sm,se,
                    capsize=2,fmt='o',color=col,ms=3)
      else:
        ax.errorbar(Z_mids,s_mean[:,jj,ii],err[:,:,jj,ii],
                    capsize=2,fmt='o',color=col,ms=3) 
      
      if fit_KLLR:
        err = np.sqrt(s_max*s_min)
        # plot_KLLR(ax,Z_mids,s_mean[:,jj,ii],err[:,jj,ii],col)
        
        if high_res:
          pars = [1000,100] # high qualtiy
        else:
          pars = [3,len(Z_mids)] # test runs
        
        plot_KLLR2(ax,scats[:,:,jj,ii],col,log=True,pars=pars)
      
    if plot_err:
      xlim = ax.set_xlim(ax.set_xlim())
      col = rd_gamma.mpl_colors[2]
      qs = np.transpose(quantiles)[ii]
      ax.fill_between(Z_meds_e,qs[med-1],qs[med+1],
                      alpha=.125,color=col,lw=0,zorder=-1)
      ax.fill_between(Z_meds_e,qs[med-2],qs[med+2],
                      alpha=.091,color=col,lw=0,zorder=-1)
    
    ax.tick_params(axis='both', which='major', labelsize=fs)
    
    if break_lines:
      # freeze y-axis
      ylim = ax.get_ylim()
      ax.set_ylim(ylim)
      # freeze x-axis
      xlim = ax.get_xlim()
      ax.set_xlim(xlim)
      for z_L,z_R in zip(lines_4kA,lines_Balmer):
        col = 'xkcd:battleship grey'
        # draw 4000 Angstrom break redshift
        ax.vlines(z_L,*ylim,col,#'dashed',
                     lw=.5,alpha=.5,zorder=-1)
        if 0:
          # draw between 4000 and 3645 Angstrom for transition region
          o = np.ones(2)
          ax.fill_between([z_L,z_R],ylim[0]*o,ylim[1]*o,
                           alpha=.125,lw=0,color=col)
  
  if cf_Hao:
    # set up general parameters
    zv = np.linspace(.1,.3,10**2)
    col = HaoEtAl2009['color']
    lbl = HaoEtAl2009['label']
    # calculate and plot scatter
    sig,sig_err = H09_sigma(zv)
    # axs[0].plot(zv,sig,color=col,lw=.5,label=lbl)
    axs[0].fill_between(zv,sig-sig_err,sig+sig_err,
                        color=col,lw=0,label=lbl,zorder=-8)#,alpha=.125)
    axs[0].legend()
  
  if cf_RM:
    # set up general parameters
    # zv = np.linspace(.05,.84)
    zv = np.linspace(0,1,10**3)
    col = RM_fit.color
    lbl = RM_fit.label
    # calculate and plot scatter
    for ii in range(D): # for each color
      sig = RM_fit.get_sigma(zv,ii)
      axs[ii].plot(zv,sig,color=col,lw=1,label=lbl,alpha=.5,zorder=-9)
    axs[0].legend(loc='lower right')
  
  plt.savefig(fdir+'bootstrap_scatt.pdf')
  if display: plt.show()
  plt.clf()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # correlations
  idx_x,idx_y = np.tril_indices(D,-1) # set up iteration tools
  fig,axs = plt.subplots(1,len(idx_x),sharex=True,sharey=True)
  axs[0].set_ylabel('Correlation')
  # axs[0].set_ylim(-1.01,1.01) # should never be |rho|>1
  axs[0].set_ylim(-.0,1.) # WARNING: hardcoded!
  for ii in range(len(idx_x)):
    ax = axs[ii]
    ax.set_xlabel("Redshift")
    for kk in range(K):
      col = rd_gamma.mpl_colors_RF[kk]
      cm,cs = meansig(corr[:,:,kk,idx_x[ii],idx_y[ii]])
      if use_median:
        cm = corr_quants[1,:,kk,idx_x[ii],idx_y[ii]]
        ce = corr_errs[:,:,kk,idx_x[ii],idx_y[ii]]
        ax.errorbar(Z_mids,cm,ce,capsize=2,fmt='o',color=col,ms=3)
      else:
        ax.errorbar(Z_mids,cm,cs,capsize=2,fmt='o',color=col,ms=3)
      lx,ly = [label_colors[v[ii]] for v in [idx_x,idx_y]]
      ax.set_title(r'$\rho(%s, \, %s)$' % (ly,lx),fontsize=fs)
      
      if fit_KLLR:
        # plot_KLLR(ax,Z_mids,cm,cs,col)
        cr = corr[:,:,kk,idx_x[ii],idx_y[ii]]
        
        if high_res:
          pars = [1000,100] # high qualtiy
        else:
          pars = [3,len(Z_mids)] # test runs
        
        plot_KLLR2(ax,cr,col,pars=pars)
    
    ax.tick_params(axis='both', which='major', labelsize=fs)
    
    if break_lines:
      # freeze y-axis
      ylim = ax.get_ylim()
      ax.set_ylim(ylim)
      # freeze x-axis
      xlim = ax.get_xlim()
      ax.set_xlim(xlim)
      for z_L,z_R in zip(lines_4kA,lines_Balmer):
        col = 'xkcd:battleship grey'
        # draw 4000 Angstrom break redshift
        ax.vlines(z_L,*ylim,col,#'dashed',
                     lw=.5,alpha=.5,zorder=-1)
        if 0:
          # draw between 4000 and 3645 Angstrom for transition region
          o = np.ones(2)
          ax.fill_between([z_L,z_R],ylim[0]*o,ylim[1]*o,
                           alpha=.125,lw=0,color=col)
  
  if 0 and cf_RM: # DEACTIVATED, since Eli doesn't trust them.
    # set up general parameters
    zv = np.linspace(.05,.84)
    col = RM_fit.color
    lbl = RM_fit.label
    # calculate and plot scatter
    for ii in range(D): # for each color
      idx = idx_x[ii],idx_y[ii]
      crr = RM_fit.get_corr(zv,idx)
      axs[ii].plot(zv,crr,color=col,lw=1,label=lbl)
    # axs[0].legend()
  
  plt.savefig(fdir+'bootstrap_corr.pdf')
  if display: plt.show()
  plt.clf()


def plot_scatrel(which='R14'):
  """
  give scatter relative to user-defined option:
  plot (scatter / `which`)
  
  Options for `which`:
   * 'R14' (default) = Rykoff+14 DES Y3 fit
   * 'RS' = red sequence
   * 'BC' = blue cloud
   * 'err' = median photometric error
  """
  ########################################################################
  # set up data to be plotted (and plotted against)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up RS and BC mean scatter and uncertainty on scatter
  lgsm,lgss = meansig(np.log10(scats))
  s_mean = 10**lgsm
  s_min,s_max = s_mean/10**lgss,s_mean*10**lgss
  err = np.array([s_mean-s_min, s_max-s_mean])
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up median redshift errors
  fname_errs = 'output/bzz_err.h5' # output filename to save errors to
  with h5py.File(fname_errs) as ds:
    keys = ['Z_bins','Z_mids','Z_meds','quantiles']
    Z_bins_e,Z_mids_e,Z_meds_e,quantiles = [ds[k][:] for k in keys]
    N_bins,N_quant,_ = np.shape(quantiles)
    med = N_quant//2 # for median index
  quants = interp1d(Z_meds_e,quantiles,axis=0,fill_value='extrapolate')(Z_mids)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up R14 fit data
  sig = np.transpose([RM_fit.get_sigma(Z_mids,ii) for ii in range(3)])
  ########################################################################
  # set up dividing factor
  div = np.ones((1,np.shape(sig)[1]))
  if which == 'R14':
    div = sig
  elif which == 'RS':
    div = s_mean[:,0]
  elif which == 'BC':
    div = s_mean[:,1]
  elif which == 'err':
    div = quants[:,med]
  """
  print("sig shape:",np.shape(sig))
  print("s_mean shape:",np.shape(s_mean))
  print("quants shape:",np.shape(quants))
  """
  ########################################################################
  # plot each component, divided by the user-defined factor
  fig,axs = plt.subplots(1,D,sharex=True)
  for ii in range(D): # for each color
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up this axis
    ax = axs[ii]
    DIV = div[:,ii]
    ax.set_xlabel('Redshift')
    ax.set_yscale('log')
    if ii == 0:
      ax.set_ylabel("Relative Scatter")
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot fits
    ax.plot(Z_mids,sig[:,ii]/DIV,color=RM_fit.color) # plot R14 fit
    for kk in range(1,3): # plot quantile errors
      ax.fill_between(Z_mids,quants[:,med-kk,ii]/DIV,quants[:,med+kk,ii]/DIV,
                       alpha=.125,lw=0,color=rd_gamma.mpl_colors[2])
    for jj in range(K): # for each component (RS & BC)
      col = rd_gamma.mpl_colors_RF[jj]
      ax.errorbar(Z_mids,s_mean[:,jj,ii]/DIV,err[:,:,jj,ii]/DIV,
                  capsize=2,fmt='o',color=rd_gamma.mpl_colors_RF[jj],ms=3) 
  plt.show()


def plot_murel(which='R14 1.75'):
  """
  give means relative to user-defined option: 
  plot (mu - `which`)
  
  Options for `which`:
   * 'R14 1.75' (default) = Rykoff+14 DES Y3 fit
   * 'R14 0.00' = Rykoff+14 DES Y3 fit
   * 'RS' = red sequence
   * 'BC' = blue cloud
  """
  ######################################################################
  # set up R14 data to match against
  cols_R14_0p00, cols_R14_1p75 = [np.transpose([RM_fit.get_mu(Z_mids,ii,
                 maglim) for ii in range(D)]) for maglim in [0.00,1.75]]
  # shapes = (32,3) for each (Z_mid,color) combination
  # mean and scatter: mu_mean,mu_sig = meansig(mu_all)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # set up subtraction
  sub = np.zeros((1,np.shape(mu_mean)[1]))
  if which in {'R14 1.75','R14'}:
    sub = cols_R14_1p75 
  elif which == 'R14 0.00':
    sub = cols_R14_0p00 
  elif which == 'RS':
    sub = mu_mean[:,0]
  elif which == 'BC':
    sub = mu_mean[:,1]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # plot each color and each component, subtracted by user-def. factor.
  fig,axs = plt.subplots(1,D,sharex=True,sharey=True)
  for ii in range(D): # for each color
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up this axis
    ax = axs[ii]
    SUB = sub[:,ii]
    ax.set_xlabel('Redshift')
    if ii == 0:
      ax.set_ylabel("Relative Color")
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot R14 fits
    col = RM_fit.color
    lss = [(0,(5,10)), '-']
    c14A,c14B = [v[:,ii]-SUB for v in [cols_R14_1p75,cols_R14_0p00]]
    ax.plot(Z_mids,c14A,color=col,ls=lss[0],lw=1) # R14
    ax.plot(Z_mids,c14B,color=col,ls=lss[1],lw=1) # R14
    ax.fill_between(Z_mids,c14A,c14B,color=col,lw=0,alpha=.0625)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot data fits
    for jj in range(K):
      col = rd_gamma.mpl_colors_RF[jj]
      mm,ms = mu_mean[:,jj,ii], mu_sig[:,jj,ii]
      axs[ii].errorbar(Z_mids,mm-SUB,ms,
                       capsize=2,fmt='o',color=col,ms=3)
  plt.show()


def avg_deviation():
  """
  compute average deviation outside of the R14 bounds (m*+1.75 to m*)
  """
  ######################################################################
  # set up R14 data to match against
  cols_R14_0p00, cols_R14_1p75 = [np.transpose([RM_fit.get_mu(Z_mids,ii,
                 maglim) for ii in range(D)]) for maglim in [0.00,1.75]]
  d0p00 = (mu_mean[:,0] - cols_R14_0p00) # upper bound
  d1p75 = (mu_mean[:,0] - cols_R14_1p75) # lower bound
  d_tot = np.max([d0p00,0*d0p00,-d1p75],axis=0) # maximum deviation
  print(f"max per color: {np.round(np.max(d_tot,axis=0),5)}")
  print(f"mean per color: {np.round(np.mean(d_tot,axis=0),5)}")
  mn = np.mean(d_tot)
  st = np.std(d_tot)
  err = st/np.sqrt(np.size(d_tot))
  print(f"mean overall: {mn:0.3g} +/- {err:0.3g}")
  print(f"st dev overall: {st:0.3g}")


def plot_murel_Nsig():
  """
  plot RS relative to R14 fits
  """
  ######################################################################
  # set up R14 data to match against
  cols_R14_0p00, cols_R14_1p75 = [np.transpose([RM_fit.get_mu(Z_mids,ii,
                 maglim) for ii in range(D)]) for maglim in [0.00,1.75]]
  d0p00 = (mu_mean[:,0] - cols_R14_0p00) / mu_sig[:,0]
  d1p75 = (mu_mean[:,0] - cols_R14_1p75) / mu_sig[:,0]
  # d_tot = np.max([d0p00,0*d0p00,-d1p75],axis=0)
  ######################################################################
  # plot significance of deviation
  fig,axs = plt.subplots(1,D,sharex=True)
  for ii in range(D):
    ax = axs[ii]
    ax.set_xlabel('Redshift')
    if ii == 0:
      ax.set_ylabel("Significance")
    # plot how significantly each color deviates from R14 expectations
    ax.scatter(Z_mids,d0p00[:,ii],label="0.00")
    ax.scatter(Z_mids,d1p75[:,ii],label="1.75")
    # ax.scatter(Z_mids,d_tot[:,0,ii],10,label='tot')
    xlim = np.array(ax.set_xlim(ax.get_xlim()))
    ax.plot(xlim,0*xlim,'k',lw=1)
  axs[0].legend()
  plt.show()


def plot_ellipses_at(Z,N=10**3,plot_points=True,bounds=False,savename=None):
  """
  for the bootstrap sample, plot all the fit ellipses at a given Z slice
  set up only for DES griz photometry
  if Z>0, find index closest to Z, else use -Z as index to plot
  bounds=False: if not False, then input following hardcoded bounds: 
                bounds = [[g-r low, g-r high],[r-i low, r-i high], ... ]
  savename=None: if not None, then save plot as that name.
  """
  # read in first red dragon to get relevant data
  ftemp = fdir + "fit_Bz_R14_2K_%03g.h5"
  rd = rd_gamma.dragon(ftemp % 0)
  if Z > 0:
    idx = rd.idx_where_Z(Z)
    Z_bin = rd._Z_bins[idx], rd._Z_bins[idx+1] 
  else:
    idx = int(-Z)
  fname = rd_gamma.fname_Bz_R14
  if plot_points: # read in underlying dataset at chosen redshift
    with h5py.File(fname,'r') as ds:
      Z = ds['Z'][:]
      mask_Z = (Z_bin[0] < Z) * (Z < Z_bin[1])
      print(f"Galaxies in bin: {len(mask_Z[mask_Z])}")
      keys = 'Z', 'Z_err', 'bands', 'bands_err'
      Z,Z_err,bands,bands_err = [ds[key][:][mask_Z] for key in keys]
      cols = -np.diff(bands,axis=1)
  # plot scatters of each of the colors
  fig,axs = plt.subplots(1,2,sharey=True)
  if plot_points:
    axs[0].scatter(cols[:N,0],cols[:N,1],1,color='k',lw=0,alpha=1/256.)
    axs[1].scatter(cols[:N,2],cols[:N,1],1,color='k',lw=0,alpha=1/256.)
    if bounds is False:
      # softcode to 3-sigma
      mn,mx = np.quantile(cols[:N],[.0027,.9973],axis=0)
      # mn,mx = np.quantile(cols[:N],[.01,.99],axis=0)
      axs[0].set_xlim(mn[0],mx[0])
      axs[0].set_ylim(mn[1],mx[1])
      axs[1].set_xlim(mn[2],mx[2])
  if bounds is not False:
    axs[0].set_xlim(bounds[0])
    axs[0].set_ylim(bounds[1])
    axs[1].set_xlim(bounds[2])
  # set up generic axes
  axs[0].set_xlabel(r'$g-r$')
  axs[0].set_ylabel(r'$r-i$')
  axs[1].set_xlabel(r'$i-z$')
  # for each bootstrap realization,
  # for ii in [0,1]:
  for ii in range(N_bootstrap):
    rd = rd_gamma.dragon(ftemp % ii)
    # plot RS and BC ellipses faintly
    for jj in range(2):
      colors = [[0,1],[2,1]][jj]
      for K in range(rd.N_fit):
        col = rd_gamma.mpl_colors_RF[K]
        sig = rd._Sigma[idx,K][np.ix_(colors,colors)]
        means = rd._mu[idx,K][colors]
        for f in [1,2]:
          rd_gamma.plot_ellipse(sig,means,f,color=col,ax=axs[jj],alpha=1/16.)
  if savename is not None:
    plt.savefig(savename)
    print(savename,"saved.")
  plt.show()


########################################################################
# 3) Use KLLR w/ errors to do weighted fitting


########################################################################
# 4) test on larger sub-samples



if __name__ == '__main__':
  if 1: # plot main results
    showplot = 1
    hr = 0
    #plot_w(fit_KLLR=1,display=showplot,high_res=hr)
    plot_mu(fit_KLLR=1,display=showplot,high_res=hr,cf_Hao=False,cf_RM=False)
    plot_scattcorr(fit_KLLR=1,display=showplot,high_res=hr,cf_Hao=False,cf_RM=False,plot_err=False)
  
  if 0:
    for which in [None,'R14','RS','BC','err']:
      plot_scatrel(which)
  if 0:
    Z = .581 # last low-error point
    Z = .605 # first high-error point
    plot_ellipses_at(Z,-1,plot_points=False)
  if 0:
    for ii in range(9):
      Z = ii/10.
      print(f"Z={Z:0.3g}")
      plot_ellipses_at(Z,-1,plot_points=False)
  if 0:
    bounds = [[.4,2], [.15,1.35], [.1,.65]]
    for Z in np.flip(np.arange(0,-32,-1)):
      f_out = fdir + f'img{Z:03g}.png'
      print(f_out)
      plot_ellipses_at(Z,-1,plot_points=False,bounds=bounds,savename=f_out)
  
  if 0: # plot relative mean colors
    for which in ['R14 0.00','R14 1.75','RS']:
      plot_murel(which)
    
    # plot_murel_Nsig()
    avg_deviation()
  
  print('~fin')
