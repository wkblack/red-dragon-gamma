# measure typical photometric errors in buzzard
import rd_gamma
from rd_gamma import *

fname_errs = 'output/bzz_err.h5' # output filename to save errors to

def calculate_color_errors(N=-1):
  " find quantiles of color errors as a function of redshift "
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in Bzz dataset 
  with h5py.File(fname_Bz_R14) as ds:
    Z = ds['Z'][:N]
    # print('Z lim:',min(Z),max(Z))
    be = ds['bands_err'][:N]
  N_gals,D = np.shape(be)
  D -= 1 # from band dimensions to color dimensions
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculate color errors
  ce = rd_gamma._band_err_to_col_err(be)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculate in redshift bins quantiles of color error
  Z_bins = np.arange(.05,1.01,.025)
  # Z_bins = np.linspace(min(Z),max(Z)+1e-16,11)
  Z_mids = (Z_bins[1:]+Z_bins[:-1])/2.
  Z_meds = np.zeros(len(Z_mids))
  # qs = [1-s2,1-s1,.5,s1,s2] # quantiles
  qs = [.5-s2/2., .5-s1/2., .5, .5+s1/2., .5+s2/2.] # quantiles
  quantiles = np.zeros((len(Z_mids),len(qs),D))
  for ii in range(len(Z_mids)):
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    Z_meds[ii] = np.median(Z[mask_ii])
    quantiles[ii] = np.quantile(ce[mask_ii],qs,axis=0)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save output 
  with h5py.File(fname_errs,'w') as ds:
    for key,arr in zip(['Z_bins','Z_mids','Z_meds','quantiles'],
                       [ Z_bins , Z_mids , Z_meds , quantiles ]):
      ds.create_dataset(key,data=arr)
    for key,val in zip(['fname','D','N_gals'],
                       [fname_Bz_R14,D,N_gals]):
      ds.attrs[key] = val
  print(fname_errs,"saved.")


def plot_color_errors():
  " plot color errors as a function of redshift from file "
  # read in data
  with h5py.File(fname_errs) as ds:
    keys = ['Z_bins','Z_mids','Z_meds','quantiles']
    Z_bins,Z_mids,Z_meds,quantiles = [ds[k][:] for k in keys]
    N_bins,N_quant,D = np.shape(quantiles)
  # set up axes
  fig,axs = plt.subplots(1,D,sharex=True,sharey=True)
  mn,mx = np.quantile(quantiles,[0,1])
  # plot data
  for ii in range(D):
    qs = quantiles[:,:,ii]
    ax = axs[ii]
    ax.set_yscale('log')
    ax.set_ylim(mn,mx)
    ax.set_xlim(Z_bins[0],Z_bins[-1])
    if 0: # quick scatters
      for jj in range(N_quant):
        col = mpl_colors[jj]
        ax.scatter(Z_mids,qs[:,jj],2,color=col)
    else: # fancy errorbars
      idx = N_quant//2
      # ax.plot(Z_mids,qs[:,idx],'k')
      ax.scatter(Z_mids,qs[:,idx],10,color='k')
      col = mpl_colors[0]
      ax.fill_between(Z_mids,qs[:,idx-1],qs[:,idx+1],alpha=.25,color=col,lw=0)
      ax.fill_between(Z_mids,qs[:,idx-2],qs[:,idx+2],alpha=.125,color=col,lw=0)
      lbl = ['$g-r$','$r-i$','$i-z$'][ii]
      ax.set_title(lbl)
  axs[0].set_ylabel('color error')
  axs[1].set_xlabel('Redshift')
  plt.show()


if __name__ == '__main__':
  # calculate_color_errors()
  plot_color_errors()
