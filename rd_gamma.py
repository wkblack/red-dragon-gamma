# including photometric errors

########################################################################
# import statements 

from time import time
import os
os.environ['OPENBLAS_NUM_THREADS'] = '12'

from os import sys
epsilon = sys.float_info.epsilon
MAX = 1/epsilon # maximum permissible value for condition number

import h5py
import sklearn

from glob import glob


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# remove some warnings
import warnings # to shut up sklearn
warnings.filterwarnings("ignore",category=DeprecationWarning)
import matplotlib.cbook # to shut up mpl
warnings.filterwarnings("ignore",category=matplotlib.cbook.mplDeprecation)

from sklearn.mixture import GaussianMixture as GM

# from matplotlib import pyplot as plt
from mpl_tools import * # import matplotlib tools and defaults
# LSC = linear segmented colormap
cmap_rgb = LSC.from_list("rgb", ['r','g','b']) # simple rgb
cmap_spec = LSC.from_list("redshift", ['r','xkcd:brick orange','g','xkcd:aqua','b']) # prettier rgb, with better middle values
vals = [0,1/6.,1/3.,.5,.75,1] # Red to Purple rainbow
cols = ['#EC4141','#EF7135','#FAF5AB','#5FBB4E','#1B98D1','#632E86']
cmap_RD = LSC.from_list("dash",list(zip(vals,cols)))

# somewhat binary coloring scheme, still keeping true to P_red value
vals = [0,.5-1e-16,.5+1e-16,1] # consider [0,.32,.68,1] for +/- 1 sigma
cols = ['xkcd:'+c for c in ['blue','light blue','light pink','red']]
cmap_RS_BC = LSC.from_list("RS/BC",list(zip(vals,cols)))


# create "red-first" mpl color cycle, preserving original otherwise
ir,ig,ib = 3,2,0 # index of red/green/blue in mpl_colors
indices_RB = np.array([ir,*range(ir),*range(ir+1,len(mpl_colors))])
indices_RGB = np.array([ir,ig,ib,1,*range(4,len(mpl_colors))])
mpl_colors_RF = np.array(mpl_colors)[indices_RB] # red first
mpl_colors_RGB = np.array(mpl_colors)[indices_RGB] # rgb first

import pygmmis


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# grab mode from scipy
import scipy
from scipy import stats

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up sigmas 
from scipy.special import erf
s1,s2,s3,s4,s5 = [erf(ii/np.sqrt(2)) for ii in range(1,6)]
qtls = .5 + np.array([-s5,-s4,-s3,-s2,-s1,0,
                      +s1,+s2,+s3,+s4,+s5])/2. 

def _Gaussian_interval(mu,sig,a,b):
  """
  _Gaussian_interval(mu,sig,a,b)
  probability that point with Gaussian error falls in [a,b] for Gaussian
  
  Parameters
  ----------
  mu,sig = mean and standard deviation of Gaussian
  a,b = interval bounds
  """
  sqrt2sig = np.sqrt(2) * np.array(sig) # for prettier calculation
  a,b,mu = [np.array(v) for v in [a,b,mu]] # ensure vector inputs
  return .5 * np.abs(erf((b-mu)/sqrt2sig) - erf((a-mu)/sqrt2sig))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set up interpolation tools
spline = scipy.interpolate.UnivariateSpline
interp1d = scipy.interpolate.interp1d
from scipy.interpolate import CubicSpline


def lin_interp(Z,v):
  """
  lin_interp(Z,v)
  
  interp1d with some defaults:
   * axis=0             # redshift axis for Red Dragon data structures)
   * assume_sorted=True # keep redshift and values sorted)
   * fill_value=(v[0],v[-1]) # use endpoints to extrapolate
   * bounds_error=False # allow extrapolation without warning
  """
  return interp1d(Z,v,axis=0,assume_sorted=True,
                  fill_value=(v[0],v[-1]),bounds_error=False)


# import KLLR for smoothing, but leave optional
try:
  from kllr import kllr_model
  from kllr import calculate_weights
  HAS_KLLR = True
except ModuleNotFoundError:
  print("Error: KLLR not found; must use other fitting")
  HAS_KLLR = False

# if KLLR found, define helper function to get counts
if HAS_KLLR:
  def get_pseudocount(x,xr,kw=.2,kt='gaussian'):
    """ return pseudo counts for each xr bin (sum of all weights at point)
    
    Parameters
    ----------
    x : data input x-values
    xr : KLLR sampled output x-points
    kw : kernel width used in KLLR analysis (default: .2)
    kt : kernel type used in KLLR analysis (default: 'gaussian')
    
    Returns
    -------
    pseudocount N, size=len(xr)
    """
    x,xr = [np.array(v) for v in [x,xr]]
    N = np.zeros_like(xr,dtype=float)
    # calculate rescaling factor to properly normalize counts per bin
    rescale = calculate_weights(np.array([0]),kt,0,kw)
    for ii in range(len(xr)):
      # sum weights of all points
      N[ii] = np.sum(calculate_weights(x,kt,mu=xr[ii],width=kw))/rescale
    return N # number of points in analysis bin


########################################################################
# set up defaults & universals 

fname_default = 'output/fit.h5'
lbl_W = r'$w$' # weight
lbl_M = r'$\left | \vec \mu \right |$' # normed color
lbl_S = r'$\left \langle \vec \sigma^2 \right \rangle^{1/2}$' # combined scatter
lbl_langle = r'$\left\langle %s \right\rangle$' # template
lbl_C = lbl_langle % r'\frac{\mu - \mu_{\rm tot}}{\sigma_{\rm tot}}' # sigmas from color mean
lbl_R = lbl_langle % r'\sigma / \sigma_{\rm tot}' # relative scatters
lbl_c = r'$c_{%i}$'

keys_data = ['Z','Z_err','bands','bands_err']


########################################################################
# general color functions 

def _band_err_to_col_err(err):
  """
  _band_err_to_col_err(err)
  calculate color errors from photometric errors
  
  Input
  -----
  err = photometric errors, size (N,N_bands)
  
  Output
  ------
  color errors, size (N,N_col)
  """
  if err is None:
    return None
  else:
    N_bands = np.shape(err)[-1]
    return np.transpose([np.sqrt(err[:,ii+1]**2 + err[:,ii]**2) \
                         for ii in range(N_bands-1)])


def _band_err_to_covar(err):
  """
  _band_err_to_covar(err)
  go from photometric band errors to noise covariance matrix
  
  Input
  -----
  err = photometric errors, size (N,N_bands)
  
  Output
  ------
  covariance matrix of color errors, size (N,N_col,N_col)
  """
  col_err = _band_err_to_col_err(err)
  N_cols = np.shape(col_err)[-1]
  diagonal = np.transpose([[col_err[:,ii]**2 * (ii==jj)
                            for ii in range(N_cols)]
                           for jj in range(N_cols)])
  off_diag = np.transpose([[-err[:,max(ii,jj)]**2 * (np.abs(ii-jj)==1) \
                            for ii in range(N_cols)] \
                           for jj in range(N_cols)])
  return diagonal + off_diag


def _corr_from_Sigma(Sigma):
  """
  _corr_from_Sigma(Sigma)
  calculate correlations from covariance matrix, 
  i.e. divide each row i and column j by sqrt(Sigma_ii*Sigma_jj)
  
  Input
  -----
  Sigma = covariance matrix, shape (...,D,D)
  """
  sigs = np.transpose(np.sqrt(np.diagonal(Sigma,0,-1,-2)))
  corr = np.copy(np.transpose(Sigma)) # must `copy` or it destroys Sigma
  D = np.shape(corr)[0] # dimensionality of color space
  for ii in range(D):
    for jj in range(D):
      corr[ii,jj] /= sigs[ii] * sigs[jj]
  return np.transpose(corr)


########################################################################
# RD class, including interpolation functions

class dragon:
  """
  Red Dragon class
  
  use: 
  rd = dragon('input.h5')
  rd.P_red(Z,bands)
  
  # front-end functions:
  rd.covar(Z)
  # back-end tools:
  """
  def __init__(self,fname):
    with h5py.File(fname,'r') as ds:
      ##################################################################
      # load core data (dragon DNA)
      Zb = self._Z_bins = ds['Z_bins'][:] # redshift bins
      self._Z_mids = (Zb[1:] + Zb[:-1])/2. # redshift midpoints
      self._w = ds['w'][:] # measured weights across redshift
      self._mu = ds['mu'][:] # measured means "
      self._Sigma = ds['Sigma'][:] # measured covariances "
      N,K,D = np.shape(self._mu) # number bins, components, dimensions
      self.N,self.K,self.D = N,K,D
      self.N_bins = N
      ##################################################################
      # filename of this dragon
      self.fname_dragon = fname
      ##################################################################
      # load attributes (and set up defaults if not included)
      def tryload(key,backup=None,from_attrs=True):
        " load key if in dataset, otherwise load backup "
        if from_attrs:
          if key in ds.attrs.keys():
            return ds.attrs[key]
        else:
          if key in ds.keys():
            return ds[key][:]
        return backup
      # number of galaxies in each Z-bin
      self.N_gals = tryload('N_gals')
      # number of Gaussian components
      self.N_fit = tryload('N_fit',K) # use data from above
      # number of colors used in fit
      self.N_cols = tryload('N_cols',D) # use data from above
      # points to exclude
      zl = np.zeros_like(self._Z_mids) # zeros at each Z-bin
      self._exclusions = tryload('exclusions',np.zeros(N)) # exclude nothing
      # filename of original dataset
      self.fname_data = tryload('fname')
      # number of galaxies used in each bin to model
      self.N_used = tryload('N_used',1) # number used to model
      # Bayesian Information Criterion at each bin
      self.BIC = tryload('BIC')
      self.BIC_err = tryload('BIC_err')
      # trained status
      self._trained = tryload('trained',False) # false by deafault
      ##################################################################
      # load uncertainties, if available
      self._w_err = tryload('w_err',from_attrs=False)
      self._mu_err = tryload('mu_err',from_attrs=False)
      self._Sigma_err = tryload('Sigma_err',from_attrs=False)
      ##################################################################
      # read in training data, if available
      if self._trained:
        self._use_weights = ds.attrs['_use_weights']
        self.tol_w = ds.attrs['tol_w']
        self._sort_factor = ds.attrs['_sort_factor']
        self._curvature_anomaly = ds['_curvature_anomaly'][:]
        # load fitting defaults
        self.method_w = ds.attrs['method_w']
        self.method_mu = ds.attrs['method_mu']
        self.method_Sigma = ds.attrs['method_Sigma']
        self.KLLR_kw = ds.attrs['KLLR_kw']
        self.softening_w_spline = ds.attrs['softening_w_spline']
        self.softening_mu_spline = ds.attrs['softening_mu_spline']
        self.softening_Sigma_spline = ds.attrs['softening_Sigma_spline']
        # load labels
        self.label_primary = ds.attrs['label_primary'] # usu redshift Z
        self.label_colors = ds.attrs['label_colors'] # usu colors: (g-r), (r-i), ...
        if 'Z_fit' in ds.keys(): # load personal fitting
          self.Z_fit = ds['Z_fit'][:]
          self.w_fit = ds['w_fit'][:]
          self.mu_fit = ds['mu_fit'][:]
          self.Sigma_fit = ds['Sigma_fit'][:]
        else:
          self.set_fits()

    if not self._trained: # set up defaults for fitting
      # set up labels
      self.label_primary = "Redshift"
      self.label_colors = [f"c_{ii}" for ii in range(self.N_cols)]
      # set up sorting factors
      self._sort_factor = -.5
      self._curvature_anomaly = np.zeros_like(self._exclusions)
      # use curvature as a weight, to soften out bad points
      self._use_weights = True
      
      # component weight parameters
      self.tol_w = 1e-3 # tolerance for f_R (exclude zeros in fit)
      # interpolation methods 
      self.method_w = 'interp1d'
      self.method_mu = 'interp1d'
      self.method_Sigma = 'interp1d' # prevent negative scatters
      
      self.KLLR_kw = Zb[2] - Zb[0] # default redshift smoothing scale
      
      if self._use_weights:
        # set sharper spline weights; curvature ignores outliers already
        self.softening_w_spline = 0.005
        self.softening_mu_spline = 0.005
        self.softening_Sigma_spline = 0.005
      else:
        # softer weights to reduce noise
        self.softening_w_spline = 0.08
        self.softening_mu_spline = 0.08
        self.softening_Sigma_spline = 0.08
      
      # only allow spline default if there are sufficient points
      N_min = 4 # minimum needed for cubic spline 
      if len(self._Z_mids) < N_min: # linear interpolation only
        self.method_w = 'interp1d'
        self.method_mu = 'interp1d'
        self.method_Sigma = 'interp1d'
      
      # TODO: figure out minimum needed for abs(fp-s)/s < tol = 0.001
      pass # TODO: alter softening to larger value to better fit this
      
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # basline dragon taming
      
      # remove nan values from arrays
      self.test_nans()
      
      """ # let people sort on their own; don't mess with input!
      # sort based on input sorting factor
      self.set_sorting(self._sort_factor)
      self.sort_from_peak()
      """
      
      # test which redshifts to exclude
      self.test_exclusions() # test old exclusions, make new exclusions
      
      # set up interpolations, splines, polyfits (relatively inexpensive)
      self.set_fits()
      
      pass # now ready to go!
  
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # saving and loading methods
  
  def save_training(self,fname_out=None):
    """
    save training of current dragon
    
    Inputs
    ------
    fname_out=None: output filename, default is to overwrite current dragon
    save_training(self,fname_out=None)
    """
    if fname_out is None:
      fname_out = self.fname_dragon
    with h5py.File(fname_out,'a') as ds:
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # save relevant training parameters
      # TODO: consider making a catch to only apply exlusions if similar shaped?
      for key,arr in zip(['w','mu','Sigma','_curvature_anomaly',
                          'Z_bins','w_err','mu_err','Sigma_err',
                          'Z_fit','w_fit','mu_fit','Sigma_fit'],
                         [self._w,self._mu,self._Sigma,self._curvature_anomaly,
                          self._Z_bins,self._w_err,self._mu_err,self._Sigma_err,
                          self.Z_fit,self.w_fit,self.mu_fit,self.Sigma_fit]):
        if key in ds.keys(): # update existing array
          ds[key][...] = arr
        else: # create new array
          if arr is not None: # w_err etc None for single run
            ds.create_dataset(key,data=arr)
      # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
      # update dataset values? 
      for key,val in zip(['_use_weights','tol_w','_sort_factor',
                      'method_w','method_mu','method_Sigma','KLLR_kw',
                      'label_primary','label_colors',
                      'N_gals', 'N_used', 'BIC', 'fname',
                      'softening_w_spline','softening_mu_spline',
                      'softening_Sigma_spline','exclusions'],
                     [self._use_weights,self.tol_w,self._sort_factor,
                      self.method_w,self.method_mu,self.method_Sigma,
                      self.KLLR_kw,self.label_primary,self.label_colors,
                      self.N_gals,self.N_used,self.BIC,self.fname_data,
                      self.softening_w_spline,self.softening_mu_spline,
                      self.softening_Sigma_spline,self._exclusions]):
        ds.attrs[key] = val
      # finally, let dragon init know this is a trained dragon
      ds.attrs['trained'] = True
    return fname_out
  
  
  def load_training(self,fname_in):
    """
    load training from file (weights, methods, softenings, ...
    """
    with h5py.File(fname_in,'r') as ds:
      self._use_weights = ds.attrs['_use_weights']
      self.tol_w = ds.attrs['tol_w']
      self._sort_factor = ds.attrs['_sort_factor']
      # labeling defaults
      self.label_primary = ds.attrs['label_primary']
      self.label_colors = ds.attrs['label_colors']
      # fitting defaults
      self.method_w = ds.attrs['method_w']
      self.method_mu = ds.attrs['method_mu']
      self.method_Sigma = ds.attrs['method_Sigma']
      self.KLLR_kw = ds.attrs['KLLR_kw']
      self.softening_w_spline = ds.attrs['softening_w_spline']
      self.softening_mu_spline = ds.attrs['softening_mu_spline']
      self.softening_Sigma_spline = ds.attrs['softening_Sigma_spline']
    print('Defaults from',fname_in,'loaded!')
  
  
  def lobotomize(self,mask_K=None,mask_D=None,f_out=None):
    """
    lobotomizes dragon, removing components and/or colors from model
    
    Inputs 
    ------
    K=None : boolean mask of which components to keep (default: ALL)
    D=None : boolean mask of which colors to keep (default: ALL)
    
    Outputs 
    ------- 
    f_out : filename of lobotomized dragon
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # check inputs
    if mask_K is not None: # check correct number of components in mask
      assert(len(mask_K)==self.K)
      mask_K = np.array(mask_K).astype(bool)
    else:
      mask_K = np.ones(self.K).astype(bool)
      mask_K = np.array(mask_K).astype(bool)
    if mask_D is not None:
      assert(len(mask_D)==self.D) # check correct dimensionality in mask
      mask_D = np.array(mask_D).astype(bool)
    else:
      mask_D = np.ones(self.D).astype(bool)
    f_in = self.fname_dragon
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # check output filename
    if f_out is None: # set up output template from current dragon
      assert(f_in[-3:]=='.h5')
      f_base = f_in[:-3]
      f_out = f_base + "_lobotomized.h5"
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # save duplicate dragon to be lobotomized
    rd = dragon(self.save_training(f_out))
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # perform lobotomization
    # implementation may be a bit verbose, but it works. 
    #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  # 
    # mask components
    rd._w = rd._w[:,mask_K]
    rd._mu = rd._mu[:,mask_K]
    rd._Sigma = rd._Sigma[:,mask_K]
    if rd._w_err is not None:
      rd._w_err = rd._w_err[:,mask_K]
    if rd._mu_err is not None:
      rd._mu_err = rd._mu_err[:,mask_K]
    if rd._Sigma_err is not None:
      rd._Sigma_err = rd._Sigma_err[:,mask_K]
    rd.w_fit = rd.w_fit[:,mask_K]
    rd.mu_fit = rd.mu_fit[:,mask_K]
    rd.Sigma_fit = rd.Sigma_fit[:,mask_K]
    #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  # 
    # mask colors
    rd._mu = rd._mu[:,:,mask_D]
    rd._Sigma = rd._Sigma[:,:,mask_D]
    rd._Sigma = rd._Sigma[:,:,:,mask_D]
    if rd._mu_err is not None:
      rd._mu_err = rd._mu_err[:,:,mask_D]
    if rd._Sigma_err is not None:
      rd._Sigma_err = rd._Sigma_err[:,:,mask_D]
      rd._Sigma_err = rd._Sigma_err[:,:,:,mask_D]
    rd.mu_fit = rd.mu_fit[:,:,mask_D]
    rd.Sigma_fit = rd.Sigma_fit[:,:,mask_D]
    rd.Sigma_fit = rd.Sigma_fit[:,:,:,mask_D]
    # and update labeling accordingly
    rd.label_colors = np.array(rd.label_colors)[mask_D]
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # save output 
    with h5py.File(rd.fname_dragon,'a') as ds:
      for key,arr in zip(['w','mu','Sigma','_curvature_anomaly',
                          'Z_bins','w_err','mu_err','Sigma_err',
                          'Z_fit','w_fit','mu_fit','Sigma_fit'],
                         [rd._w,rd._mu,rd._Sigma,rd._curvature_anomaly,
                          rd._Z_bins,rd._w_err,rd._mu_err,rd._Sigma_err,
                          rd.Z_fit,rd.w_fit,rd.mu_fit,rd.Sigma_fit]):
        del ds[key] # OVERWRITE existing array, as it's changed shape.
        ds.create_dataset(key,data=arr)
      for key,val in zip(['label_colors'],
                         [rd.label_colors]):
        ds.attrs[key] = val
    # return filename of lobotomized dragon
    return f_out
  
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # other methods
  
  def __str__(self):
    " simple to-string method "
    return f"{self.N_fit}K {self.N_cols}D"
  
  
  def to_string(self):
    " verbose to-string method "
    return f"<{self.N_fit}K {self.N_cols}D Red Dragon, trained from {self.fname_data}>"
  
  
  def idx_where_Z(self,Zv):
    """
    idx_where_Z(self,Zv)
    
    prints index where redshift is closest to Zv
    """
    # if single value given, turn into array for processing
    if not hasattr(Zv,'__len__'):
      Zv = [Zv]
    # find nearest redshifts for each value input
    report = []
    for zv in Zv:
      dZ = np.abs(self._Z_mids-zv)
      report += [np.argwhere(dZ==min(dZ))[-1]]
    return np.squeeze(report)
  
  
  def _calc_curvature_anomaly(self,which=1):
    """
    returns curvature anomaly from means
    which=1: which of [w,mu,Sigma] to use for curvature; 
             currently only mu programmed
    """
    mask = ~self._exclusions.astype(bool)
    if len(mask[mask]) < 2:
      print("ERROR: too many exclusions; ignoring all exclusions.")
      mask = np.ones_like(mask,dtype=bool)
    if len(mask[mask]) < 4: # not enough info to calculate curvature
      print("Too few points to calculate curvature anomaly.")
      return np.ones(len(mask))
    val = [self._w,self._mu,self._Sigma][which][mask]
    second = np.zeros_like(val)
    ii = np.arange(np.shape(val)[0] - 2) # indices, excluding endpoints
    # finite difference second derivative
    second[1:-1] = val[ii] - 2 * val[ii+1] + val[ii+2]
    second[0] = second[1] # start = second
    second[-1] = second[-2] # endpoint = penultimate
    mn,st = [f(second,axis=0) for f in [np.mean,np.std]]
    sigs = (second - mn)/st
    # TODO: edit this to allow w or Sigma anomaly
    sig_tot = np.sqrt(np.sum(sigs**2,axis=(-2,-1))) 
    factor = np.sqrt(self.N_cols)
    # return report
    report = np.zeros(len(mask))
    report[mask] = sig_tot/factor
    return report
  
  
  def set_fits(self):
    " set up fits (linear, spline, KLLR) "
    mask = ~np.array(self._exclusions).astype(bool)
    if len(mask[mask]) < 2:
      print("ERROR: too many exclusions; ignoring all exclusions.")
      mask = np.ones_like(self._exclusions).astype(bool)
    Z = self._Z_mids[mask]
    Z_lim = self._Z_bins[0], self._Z_bins[-1]
    self.Z_fit = np.linspace(*Z_lim,1000)
    w,mu,Sigma = [v[mask] for v in [self._w,self._mu,self._Sigma]]
    corr = _corr_from_Sigma(Sigma)
    # read in uncertainties, if available.
    w_err,mu_err,Sigma_err = self._w_err,self._mu_err,self._Sigma_err
    if w_err is not None:
      w_err = w_err[mask]
    if mu_err is not None:
      mu_err = mu_err[mask]
    if Sigma_err is not None:
      Sigma_err = Sigma_err[mask]
      lg_var_err = np.log(np.diagonal(Sigma_err,axis1=-1,axis2=-2))
      corr_err = _corr_from_Sigma(Sigma_err)
    else:
      lg_var_err,corr_err = None, None
    # check that no nans used in fitting
    for v in [w,mu,Sigma]:
      assert(~np.any(np.isnan(v)))
    # create sqrt(N_gals used) for uncertainty weighting
    if hasattr(self.N_used,'__len__'):
      sqt_N = np.sqrt(self.N_used[mask])
    else:
      print("WARNING: N_used not set!")
      sqt_N = 1
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # linear interpolation between points (interp1d)
    self.w_fit = lin_interp(Z,w)(self.Z_fit)
    self.mu_fit = lin_interp(Z,mu)(self.Z_fit)
    self.Sigma_fit = lin_interp(Z,Sigma)(self.Z_fit)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # spline interpolation
    if self._use_weights:
      ca = self._curvature_anomaly = self._calc_curvature_anomaly(1) # mu
      wts = (1 - erf(ca/np.sqrt(2)))[mask]
    else:
      wts = np.ones_like(Z)
    if (self.N_used is not None) and (np.sum(self.N_used)>0):
      wts *= sqt_N/np.max(sqt_N)
    
    def spline_fit(y,s,idx):
      """
      spline fitting function 
      normalize scaling factor by yrange
      """
      ysig = np.mean(np.diff(np.nanquantile(y,[1-s2,.5,s2])))
      assert(ysig>0)
      return spline(Z,y[:,idx],wts,s=s*ysig)(self.Z_fit)
    
    
    if self.method_w == 'spline': # set weights
      s = self.softening_w_spline
      self.w_fit = np.transpose([spline_fit(w,s,ii)\
                                 for ii in range(self.N_fit)])
    
    if self.method_mu == 'spline': # set means
      s = self.softening_mu_spline
      self.mu_fit = np.transpose([[spline_fit(mu[:,:,jj],s,ii) 
                          for ii in range(self.N_fit)] \
                         for jj in range(self.N_cols)])
    
    if self.method_Sigma == 'spline': # set covariances
      s = self.softening_Sigma_spline
      for ii in range(self.N_fit):
        for jj in range(self.N_cols): # set up scatters
          v = spline_fit(np.log(Sigma[:,:,jj,jj]),s,ii)
          self.Sigma_fit[:,ii,jj,jj] = np.exp(v)
        for jj in range(self.N_cols): # set up correlations
          for kk in range(jj+1,self.N_cols): # for each off-diagonal
            v = spline_fit(corr[:,:,jj,kk],s,ii)
            v = np.clip(v,-1,1) # enforce real correlations
            var = np.sqrt(self.Sigma_fit[:,ii,jj,jj]*self.Sigma_fit[:,ii,kk,kk])
            self.Sigma_fit[:,ii,jj,kk] = self.Sigma_fit[:,ii,kk,jj] = v * var
    

    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # KLLR interpolation 
    
    xlim = [self._Z_bins[:-1][mask][0], # limits of extrapolation
            self._Z_bins[1:][mask][-1]]
    N_bins = 500
    
    
    def KLLR_fit(y,kw,err=1/sqt_N):
      " fit various data y to evolve smoothly along the primary axis "
      lm = kllr_model(kernel_width=kw)
      with warnings.catch_warnings():
        # since scatter ~0 here, will have issues calculating scatter,
        # but we're not using scatter---just the xl,yl smoothed fit.
        warnings.simplefilter("ignore")
        xl,yl,ic,sl,sc,sk,kt = out = lm.fit(Z,y,xrange=xlim,bins=N_bins,
                                            nBootstrap=1,y_err=err)
      # xr,yrm,icpt,sl,sig = lm.fit(Z,y,nbins=N_bins,xrange=xlim)
      # N = get_counts(Z,xr,kw); err = sig/np.sqrt(N)
      # TODO: return error of the mean?
      return xl,yl
    
    if (self.method_w == 'KLLR') and HAS_KLLR: # set weights
      yvs = np.zeros((N_bins,self.N_fit))
      for ii in range(self.N_fit):
        e = None if w_err is None else w_err[:,ii]
        xv,yv = KLLR_fit(w[:,ii],self.KLLR_kw,e)
        yvs[:,ii] = yv
      self.w_fit = lin_interp(xv,yvs)(self.Z_fit)
    
    if (self.method_mu == 'KLLR') and HAS_KLLR: # set means
      yvs = np.zeros((N_bins,self.N_fit,self.N_cols))
      for ii in range(self.N_fit):
        for jj in range(self.N_cols): 
          e = None if mu_err is None else mu_err[:,ii,jj]
          xv,yv = KLLR_fit(mu[:,ii,jj],self.KLLR_kw,e)
          yvs[:,ii,jj] = yv
      self.mu_fit = lin_interp(xv,yvs)(self.Z_fit)
    
    if (self.method_Sigma == 'KLLR') and HAS_KLLR: # set covariances
      yvs = np.zeros((N_bins,self.N_fit,self.N_cols,self.N_cols))
      for ii in range(self.N_fit):
        for jj in range(self.N_cols): 
          # calculate scatter fit (prevent negative fits)
          e = None if lg_var_err is None else lg_var_err[:,ii,jj]
          xv,yv = KLLR_fit(np.log(Sigma[:,ii,jj,jj]),self.KLLR_kw,e)
          yvs[:,ii,jj,jj] = np.exp(yv)
        for jj in range(self.N_cols):
          for kk in range(jj+1,self.N_cols): 
            var = np.sqrt(Sigma[:,ii,jj,jj]*Sigma[:,ii,kk,kk])
            rho = Sigma[:,ii,jj,kk]/var
            e = None if corr_err is None else corr_err[:,ii,jj,kk]
            xv,yv = KLLR_fit(rho,self.KLLR_kw,e)
            yv = np.clip(yv,-1,1) # enforce real correlations
            var = np.sqrt(yvs[:,ii,jj,jj]*yvs[:,ii,kk,kk])
            yvs[:,ii,jj,kk] = yvs[:,ii,kk,jj] = yv * var
      self.Sigma_fit = lin_interp(xv,yvs)(self.Z_fit)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # end of fit setting
    return
  
  
  def get_WCR(self):
    """
    returns for each component: 
     * weights, 
     * color sigmas from mean (mu / sigma_tot), 
     * and relative scatter (sigma / sigma_tot)
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # return weights
    W = self._w + epsilon
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # calculate variance for each component and color
    var = np.diagonal(self._Sigma,axis1=-1,axis2=-2) + epsilon
    # estimate total variance of sample (weighted to nix outliers)
    var_tot = np.sum(var*W[:,:,np.newaxis],axis=1) + epsilon
    # compute average fraction of the total variance for each component
    rel_var = np.sum(var/var_tot[:,np.newaxis,:],axis=-1)/var.shape[-1]
    rel_sig = np.mean(np.sqrt(var/var_tot[:,np.newaxis,:]),axis=-1)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # weighted mean c(Z) for each color
    mean_cols = np.sum(self._w[:,:,np.newaxis]*self._mu,axis=1)
    cols_sub = self._mu - mean_cols[:,np.newaxis,:] # color minus mean color
    # average across each color
    col_sigs = np.mean([cols_sub[:,:,ii]/np.sqrt(var_tot[:,ii,np.newaxis]) for ii in range(self.N_cols)],axis=0)
    return W,col_sigs,rel_sig
  
  
  def get_flat_pars(self):
    """
    returns array with all flattened fit parameters:
    >>> np.shape(rd.get_flat_pars()) = [rd.N, rd.K, 1 + 3 * rd.D]
    
    the ordering of fit parameters is:
      {weight, mean colors, log variances, correlations}
    where each of those represent the actual values fitted
    """
    lgvars = np.log(np.diagonal(self._Sigma,0,-1,-2))
    idxs = np.tril_indices(self.D,-1)
    corrs = np.transpose([_corr_from_Sigma(self._Sigma)[:,:,ixl,ixr] 
                          for ixl,ixr in zip(*idxs)],axes=[1,2,0])
    # stitch together
    return np.array([[[subitem for items in [x[ii,jj].flatten() 
                     for x in [self._w, self._mu, lgvars, corrs]] 
                      for subitem in items] 
                       for jj in range(self.K)] 
                        for ii in range(self.N)])
  
  
  def get_flat_pars_err(self):
    """ 
    return uncertainties of `get_flat_pars` routine. 
    """
    if (self._Sigma_err is None):
      return None
    lgvars = np.log(np.diagonal(self._Sigma_err,0,-1,-2))
    idxs = np.tril_indices(self.D,-1)
    corrs = np.transpose([_corr_from_Sigma(self._Sigma_err)[:,:,ixl,ixr] 
                          for ixl,ixr in zip(*idxs)],axes=[1,2,0])
    # stitch together
    return np.array([[[subitem for items in [x[ii,jj].flatten() 
                     for x in [self._w_err, self._mu_err, lgvars, corrs]] 
                      for subitem in items] 
                       for jj in range(self.K)] 
                        for ii in range(self.N)])
  
  
  def get_flat_pars_labels(self):
    """
    returns array with all flattened fit parameter labels, 
    with contents: {weight, colors, ln variances, correlations}
    in appropriate nomenclature
    """
    lbl_w = [r'$w$']
    lc = self.label_colors
    lbl_mu = [r'$\langle %s \rangle$' % c for c in lc]
    lbl_lgvar = [r'$\lg {\sigma_{%s}}^2$' % c for c in lc]
    idxs = np.tril_indices(self.D,-1)
    lbl_corr = [f'$\\rho({lc[r]},{lc[l]})$' for l,r in zip(*idxs)]
    label_sets = [lbl_w,lbl_mu,lbl_lgvar,lbl_corr]
    return [lbl for lbls in label_sets for lbl in lbls]


  def test_nans(self):
    " exclude any nan values from fitting "
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # check for nan values
    nans = np.any(np.isnan(self._w),axis=(1)) \
         + np.any(np.isnan(self._mu),axis=(1,2)) \
         + np.any(np.isnan(self._Sigma),axis=(1,2,3))
    self._exclusions[nans] = 1
  
  
  def _get_distinctness(self):
    """
    looking at neighboring points in {w,mu,Sigma}, 
    find which areas have the most distinct values, 
    i.e. where it would be least ambiguous to match components. 
    """
    # set up function to measure for some (len(Z),K) sample
    # how far away things are from discrete neighboring bins
    def distinctness(X):
      " generate arbitrary measure of distinctness "
      sort = np.argsort(X,axis=1)
      X_new = np.array([X[ii][sort[ii]] for ii in range(len(X))])
      d0 = np.abs(np.diff(X_new,axis=0))
      d1 = np.abs(np.diff(X_new,axis=1))
      # reformulate values for easy division
      z0 = np.zeros((1,d0.shape[1]))
      d0_max = np.max([np.append(z0,d0,axis=0),
                       np.append(d0,z0,axis=0)],axis=0) + epsilon
      d1_min = np.min([np.append(d1[:,0][:,np.newaxis],d1,axis=1),
                       np.append(d1,d1[:,-1][:,np.newaxis],axis=1)],axis=0)
      # return metric of distincness of sample
      return np.min(d1_min/d0_max,axis=1)
    
    # measure distinctness for weights, relative color, and scatter
    W,C,R = self.get_WCR()
    dW = distinctness(np.log10(W))
    dC = distinctness(C)
    dR = distinctness(np.log10(R))
    # if one elment is very distinct, it's more useful, 
    # so return mean (rather than median / max / min)
    return np.mean([dW,dC,dR],axis=0)
  
  
  def _get_smooth_distinctness(self,factor=.50):
    " return smoothed version of distinctness "
    def sum_near(x,f=.5):
      " give a weighted sum, decreasing contributions further out "
      L = len(x)
      x[np.isnan(x)] = 0
      report = np.zeros(L)
      for ii in range(L):
        for a in range(ii,L):
          if ~np.isnan(x[a]):
            report[ii] += x[a] * f**(a-ii)
        for b in range(0,ii):
          if ~np.isnan(x[b]):
            report[ii] += x[b] * f**(ii-b)
      return report
    
    d = self._get_distinctness()
    d_smooth = sum_near(d,factor) # smoothed distinctness
    return d_smooth / max(d_smooth) * max(d)
  
  
  def sort_from_peak(self,order=None,factor=0,printing=False):
    """
    find most distinct peak of dragon DNA across all Z values, 
    then start from there and work out left and right to match
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # determine peak index
    d = self._get_distinctness()
    d = d_smooth = self._get_smooth_distinctness()
    idx_peak = np.nanargmax(d)
    idx_max = len(d) # maximum index
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set peak point
    if order is None:
      W,C,R = np.copy(self.get_WCR()) # pull values to use in sorting
      order = np.flip(np.argsort(C[idx_peak] - factor*R[idx_peak]))
    else:
      assert(len(order)==self.N_fit)
    
    if printing:
      print('first sort:',idx_peak,self._Z_mids[idx_peak],order)
    self.sort(idx_peak,order) # sort peak point
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set neighboring points
    # first create set of all possible permutations
    from itertools import permutations
    p = np.array(sorted(set(permutations(range(self.N_fit)))))
    Np = len(p) # total number of unique permutations of sorting
    # geometrical mean, for use in following function
    geomean = lambda x: np.exp(np.mean(np.log(x),axis=0))
    
    def best_permutation(idx_cf,idx_permute):
      """
      best_permutation(idx_cf,idx_permute)
      find best sorting to use between two indices
      idx_cf = baseline for comparison; static
      idx_permute = index to cycle through all permutations
      returns permutation indices which optimize pairing, 
      best making idx_permute match idx_cf.
      """
      # try all permuations of sorting, see which has least error
      W,C,R = np.copy(self.get_WCR()) # pull values to use in sorting
      eps = 1e-10 # small increment to prevent errors with logs
      W = np.log10(W+eps); R = np.log10(R+eps) # for comparison
      eW,eC,eR = np.zeros((3,len(p)))
      for ii in range(len(p)):
        # calculate issues induced or solved with swapping
        eW[ii] = np.sum((W[idx_cf] - W[idx_permute][p[ii]])**2)
        eC[ii] = np.sum((C[idx_cf] - C[idx_permute][p[ii]])**2)
        eR[ii] = np.sum((R[idx_cf] - R[idx_permute][p[ii]])**2)
      # combined, scaled so minimum error given unit scale
      # errs = np.mean([v/min(v) for v in [eW,eC,eR]],axis=0)
      errs = geomean([eps + v/(eps + min(v)) for v in [eW,eC,eR]])
      return p[np.argmin(errs)]
    
    if printing:
      print()
    idx_curr = idx_peak
    
    while idx_curr > 0: # sort neighbors to left
      if printing:
        print(idx_curr-1,self._Z_mids[idx_curr-1])
      # set left neighbor to match, based on proximity
      best_sort = best_permutation(idx_curr,idx_curr-1)
      if printing: 
        print(best_sort)
      self.sort(idx_curr-1,best_sort)
      idx_curr -= 1
    
    if printing: 
      print()
    idx_curr = idx_peak
    
    while idx_curr+1 < idx_max: # sort neighbors to right
      if printing: 
        print(idx_curr+1,self._Z_mids[idx_curr+1])
      # set right neighbor to match, based on proximity
      best_sort = best_permutation(idx_curr,idx_curr+1)
      if printing: 
        print(best_sort)
      self.sort(idx_curr+1,best_sort)
      idx_curr += 1
    
    self.set_fits()
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # TODO: consider adding curvature minimization here as 2nd pass
    pass
  
  
  def get_rank(self):
    " returns ranking of each indicator: which is clearest "
    # set up vertical and horizontal differences 
    flat = self.get_flat_pars()
    N_bins,N_fit,N_pars = flat.shape
    sort_flat = np.sort(flat,axis=1)
    # calculate vertical difference (between components) 
    d_v = np.diff(sort_flat,axis=1)
    dv_min = np.min(d_v,axis=1)
    # calculate horizontal difference (better Z-bins) 
    d_h = np.abs(sort_flat[1:] - sort_flat[:-1])
    dh_left = np.zeros_like(sort_flat)
    dh_right = np.zeros_like(sort_flat)
    dh_left[:-1] = d_h
    dh_right[1:] = d_h
    dh_max = np.max([dh_left,dh_right],axis=(0,2))
    # calculate rank of each indicator in each bin
    rank = dv_min / dh_max
    return rank
  
  
  def sort_from_rank(self,rank,printing=False):
    """
    given input ranking of parameters, 
    
    """
    ####################################################################
    # find best starting parameter
    max_rank = np.max(rank,axis=1) # highest score across pars
    idx_start = np.min(np.argmax(max_rank)) # best starting index
    idx_best_par = np.argmax(rank,axis=1) # best sorting parameter
    ####################################################################
    # for each Z-bin (left of peak, then right of peak), 
    # figure out which parameter is most discerning, 
    # then use that parameter to sort out the neighboring bin. 
    if printing:
      print(f"Sorting from peak: Z_mids[{idx_start}] = {self._Z_mids[idx_start]:0.10g}")
    flat = self.get_flat_pars()
    N_bins,N_fit,N_pars = flat.shape
    index_sets = [np.flip(np.arange(idx_start)), # left from peak
                  np.arange(idx_start+1,N_bins)] # right from peak
    d_idxs = [+1,-1] # leftward and rightward old index locations
    for d_idx,indices in zip(d_idxs,index_sets):
      for idx in indices:
        idx_par = idx_best_par[idx] # ideal parameter to sort index `idx`
        oldsort = np.argsort(flat[idx+d_idx,:,idx_par]) # K-ordering
        currsort = np.argsort(flat[idx,:,idx_par])
        if not np.all(oldsort==currsort):
          if printing:
            print(f"Sorting Z_mids[{idx}] = {self._Z_mids[idx]:0.10g}")
            print(f"using parameter {idx_par}")
            print(f"using both {idx} and {idx+d_idx}")
          # set up mask needed to turn currsort into oldsort ordering
          sort = currsort[oldsort.argsort()]
          if printing:
            print(idx,oldsort,currsort,sort)
          flat[idx,:] = flat[idx,sort] # sort current version of `flat`
          self.sort(idx,sort) # sort internal parameters
    if printing:
      print("Completed sorting!")
  
  
  def superior_sort(self,printing=False):
    """
    use smoothed rank to best sort.
    """
    flat = self.get_flat_pars()
    N_bins,N_fit,N_pars = flat.shape
    rank = self.get_rank()
    rank_smooth = np.log10(rank) # logged copy to be smoothed
    for ii in range(N_pars):
      rank_smooth[:,ii] = smooth(rank_smooth[:,ii])
    rank_smooth = 10**rank_smooth
    self.sort_from_rank(rank_smooth,printing)
  
  
  def minimize_curvature(self,printing=False):
    """
    use point-by-point curvature between WCR to get more consistent fits
    printing=False: toggle to display scoring for each component
    """
    # structure: 
    # go point by point (starting w/ point 2), 
    # calculate curvature w/ various sortings
    # see which points in WCR are closest to their neighbors
    # guess which one goes to which by optimizing the sorting point by point
    mask = ~self._exclusions.astype(bool)
    W,C,R = np.copy(self.get_WCR())[:,mask,:]
    N = self.N_fit
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # function to distribute preferences
    def set_pref(preferences):
      " take preferences, score them, then assign a preffered sorting "
      def score(matr):
        matr = np.array(matr)
        scores = np.zeros(N)
        for ii in range(3): # for each of w,mu,Sigma
          for jj in range(N): # for each rank
            scores[matr[ii,jj].astype(int)] += N**(N-1-jj)
        return scores
      all_scores = np.array([score(preferences[ii]) for ii in range(N)])
      if printing:
        print(all_scores)
      # find max scores and assign accordingly
      handling_order = np.argsort(-np.max(all_scores,axis=1))
      # assign each to its favorite, so long as that favorite isn't taken. 
      sort = -np.ones(N)
      for ii in range(N): # for each component
        for jj in range(N): # for each ranking
          v = np.argsort(all_scores[handling_order[ii]])[jj]
          if v in sort:
            continue
          else:
            sort[handling_order[ii]] = v
            # TODO: recompute h_o, excluding scores already used
      return sort.astype(int)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # first point already set; use as default
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # second point: flat extrapolation
    # look at which index suits each best
    preference = np.zeros((N,3,N)) # rankings
    for ii in range(N): # for each component
      for jj in range(3): # for each of W,C,R
        diff = np.abs([W,C,R][jj][1]-[W,C,R][jj][0][ii])
        preference[ii][jj] = np.argsort(diff) # give component ranks
    sorting = set_pref(preference)
    # swap as needed
    idx = np.arange(len(mask))[mask][1]
    W[1] = W[1][sorting]
    C[1] = C[1][sorting]
    R[1] = R[1][sorting]
    self._w[idx] = self._w[idx][sorting]
    self._mu[idx] = self._mu[idx][sorting]
    self._Sigma[idx] = self._Sigma[idx][sorting]
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # third point to end: linear extrapolation
    # minimize distance from linearly extrapolated values
    for kk in range(len(W)-1):
      idx = np.arange(len(mask))[mask][kk+1]
      if printing:
        print(kk,self._Z_mids[idx])
      preference = np.zeros((N,3,N)) # rankings
      for ii in range(N):
        for jj in range(3): # each of W,C,R
          # better than curvature for very noisy data
          diff = np.abs([W,C,R][jj][kk+1]-[W,C,R][jj][kk][ii])
          preference[ii][jj] = np.argsort(diff)
          if printing:
            print(ii,jj,diff,np.argsort(diff))
      if printing:
        print(preference)
      sorting = set_pref(preference)
      if printing:
        print(sorting,'\n')
      assert(set(sorting) == set(range(N)))
      # swap as needed
      W[kk+1] = W[kk+1][sorting]
      C[kk+1] = C[kk+1][sorting]
      R[kk+1] = R[kk+1][sorting]
      self._w[idx] = self._w[idx][sorting]
      self._mu[idx] = self._mu[idx][sorting]
      self._Sigma[idx] = self._Sigma[idx][sorting]
  
  
  def test_exclusions(self,mask_anomalies=False):
    """
    look at weights, means, &c. and make additional exclusions,
    updating self._exclusions
    """
    self.test_nans() # if haven't already
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # TODO: be more rigorous here! 
    # just in general, the following will only find /single/ errant points---
    # these methods won't find /groups/ of errant points. 
    pass
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # jackknife error -> sigmas from line when excluded from fit?
    pass
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # check second derivatives in mean color
    ca = self._curvature_anomaly = self._calc_curvature_anomaly(1) # mu
    mask_odd = ca > 3
    mask_err = ca > 5
    if mask_anomalies:
      self._exclusions[mask_err] = 1
    if len(mask_odd[mask_odd]) > 0:
      print("Possible problems (large local 2nd deriv in colors):")
      print(f"Odd redshifts:",np.round(self._Z_mids[mask_odd],5))
      print(f"Errant redshifts:",np.round(self._Z_mids[mask_err],5))
  
  
  def sort(self,Z,sort):
    """
    sort(Z,sort)
    re-sorts at each index `Z` by sorting schema `sort`
    
    e.g. for a 3-component model, if we wanted to swap elements 2 and 3
    for the last three redshift bins, then we'd do: 
    >>> rd.sort([-3,-2,-1],[0,2,1])
    >>> rd.save_training()
    
    rearranges [k0,k1,k2] = [k0,k1,k2][sort], 
    so here k0 becomes sort[0],
            k1 becomes sort[1], and
            k2 becomes sort[2].
    """
    self._w[Z] = self._w[Z,sort]
    if self._w_err is not None:
      self._w_err[Z] = self._w_err[Z,sort]
    self._mu[Z] = self._mu[Z,sort]
    if self._mu_err is not None:
      self._mu_err[Z] = self._mu_err[Z,sort]
    self._Sigma[Z] = self._Sigma[Z,sort]
    if self._Sigma_err is not None:
      self._Sigma_err[Z] = self._Sigma_err[Z,sort]
    return
  
  
  def set_sorting(self,factor=-.5):
    """
    set_sorting(self,factor=-.5)
    to determine continuity of components across redshift, 
    sorts between slices via [C + factor * R] (see get_WCR)
    """
    W,C,R = self.get_WCR()
    sort = np.flip(np.argsort(C + factor * R),axis=-1)
    # sort w, mu, Sigma accordingly 
    self._w = np.array([v[s] for v,s in zip(self._w,sort)])
    self._mu = np.array([v[s] for v,s in zip(self._mu,sort)])
    self._Sigma = np.array([v[s] for v,s in zip(self._Sigma,sort)])
    # update fits with new sorting
    self.set_fits()
    # update value of sorting factor
    self._sort_factor = factor
    return
  
  
  ######################################################################
  # calculate fits
  
  def get_weights(self,Z):
    """
    get_weights(self,Z)
    returns weights w(Z) for given redshift(s)
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # calculate fit
    report = lin_interp(self.Z_fit,self.w_fit)(Z)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # clip to fit within (0,1); literal zero yields error
    report = np.clip(report,0+self.tol_w,1-self.tol_w)
    if hasattr(Z,'__len__'):
      return report/np.sum(report,axis=-1)[:,np.newaxis]
    else:
      return report/np.sum(report,axis=-1)
  get_w = get_weights
  
  
  def get_means(self,Z): # ,method=None,softening=.035):
    """
    returns means mu(Z) for given redshift(s) 
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # calculate fit
    report = lin_interp(self.Z_fit,self.mu_fit)(Z)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # no touch-up work to be done on mean colors; return report
    return report
  get_mu = get_means
  
  
  def get_covars(self,Z):
    """
    returns covariance matrix Sigma(Z) for given redshift(s) 
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    report = lin_interp(self.Z_fit,self.Sigma_fit)(Z)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    # consider doing consistency checks here
    return report
  get_Sigma = get_covars
  
  
  def get_scatt_vals(self,Z):
    " return scatters for given redshift(s) "
    return np.sqrt(np.diagonal(self.get_covars(Z),0,-1,-2))
  
  
  def get_corr_vals(self,Z):
    " return correlation matrix for given redshift(s) "
    # weight by each component
    Sigma = self.get_covars(Z) # shape: (len(Z),N_fit,N_col,N_col)
    return _corr_from_Sigma(Sigma)
  
  
  ######################################################################
  # estimate component membership probabilities 
  
  def get_gmm(self,Z):
    """
    get_gmm(self,Z)
    return pygmmis Gaussian mixture model object at the given redshift
    """
    gmm = pygmmis.GMM(self.N_fit,self.N_cols)
    gmm.amp = self.get_weights(Z)
    gmm.mean = self.get_means(Z)
    gmm.covar = self.get_covars(Z)
    return gmm
  
  
  def _get_logL_slice(self,Z_med,bands,bands_err=None):
    " helper method to handle slice of data for get_logL "
    # grab Gaussian mixture for this redshift
    X = -np.diff(bands,axis=1) # color measurements
    # test valid input: same number of colors for training/measuring
    if X.shape[1] != self.N_cols:
      message = f"Dragon trained on {self.N_cols} colors, "
      message += f"but received {X.shape[1]} colors."
      raise ValueError(message)
    gmm = self.get_gmm(Z_med)
    # if errors available, set up covariance in errors
    if bands_err is not None:
      covar = _band_err_to_covar(bands_err)
    else: 
      covar = None
    # calculate log likelihoods for each component
    logLs = np.array([gmm.logL_k(ii,X,covar) for ii in range(gmm.K)])
    return logLs
  
  
  def get_logL(self,Z,bands,bands_err=None,dZ=.01):
    """
    returns likelihoods for each component given redshifts, bands, err
    get_logL(Z,bands,bands_err=None,dZ=.01)
    
    Z : median redshift or redshift for each galaxy
    bands : photometry for each galaxy
    bands_err : photometric error for each galaxy
    dZ=.01 : maximum width permissable for binned color analysis
    
    For samples with redshifts for each galaxy (i.e. redshift-evolving
    lightcone, rather than a think redshift slice), RD uses bins of
    width dZ to analyze chunks of galaxies at a time. 
    """
    # set up redshift bins
    Z_lim = np.quantile(Z,[0,1])
    if np.diff(Z_lim) < dZ:
      Z = np.median(Z) # condense to single redshift bin
    if hasattr(Z,'__len__'): # handle a series of redshifts
      # break up data into series of small redshift bins, width dZ
      # (Don't actually use N GMMs!)
      N_bins = int(np.ceil(np.diff(Z_lim)/dZ))
      Z_bins = np.linspace(*Z_lim,N_bins+1)
      # set up report variable for outputs
      logLs = np.zeros((self.N_fit,len(Z))) # for each data point
      for ii in range(N_bins):
        mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
        if len(mask_ii[mask_ii]) < 1:
          continue
        Z_ii = np.median(Z[mask_ii])
        err_ii = None if bands_err is None else bands_err[mask_ii]
        Ls_ii = self._get_logL_slice(Z_ii,bands[mask_ii],err_ii)
        logLs[:,mask_ii] = Ls_ii
      return logLs
    else: # use single redshift bin
      return self._get_logL_slice(Z,bands,bands_err)
  
  
  def get_pred(self,Z,bands,bands_err=None,dZ=.01):
    """
    returns index of maximum likelihood component
    get_pred(self,Z,bands,bands_err=None,dZ=.01)
    """
    logLs = self.get_logL(Z,bands,bands_err,dZ)
    return np.argmax(logLs,axis=0)
  
  
  def get_P(self,Z,bands,bands_err=None,dZ=.01):
    """
    returns component membership probabilities for each input galaxy
    get_pred(self,Z,bands,bands_err=None,dZ=.01)
    """
    logLs = self.get_logL(Z,bands,bands_err,dZ)
    denominator = np.sum(np.exp(logLs),axis=0)
    denominator[denominator==0] = 1
    return np.array([np.exp(logLs[ii])/denominator \
                    for ii in range(self.N_fit)])
  
  
  def P_red(self,Z,bands,bands_err=None,dZ=.01):
    """
    returns RS membership probability
    P_red(self,Z,bands,bands_err=None,dZ=.01)
    """
    return self.get_P(Z,bands,bands_err,dZ)[0]
  
  
  ######################################################################
  # use RD to estimate 1D cut extents
  
  def wyvren_bounds(self,Z):
    """
    hard cut values from RD DNA to select RS
    wyvren_bounds(self,Z)
    
    returns (RS lower bounds, RS upper bounds), shape (2,len(Z),N_col)
    """
    # reformat redshift
    if not hasattr(Z,'__len__'):
      Z = np.array([Z])
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # grab each part of the Gaussian mixture
    w = self.get_weights(Z)
    mu = self.get_means(Z)
    Sigma = self.get_covars(Z)
    stds = np.sqrt(np.diagonal(Sigma,0,-1,-2)) # grab scatters of each
    # select RS and BC nominal components
    w0,w1 = w[:,0], w[:,1]
    mu_RS,mu_BC = mu[:,0], mu[:,1]
    sig_RS,sig_BC = stds[:,0], stds[:,1]
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # set up report (it's long)
    # 1) square root terms:
    A = (w0/w1)[:,np.newaxis] * sig_BC/sig_RS
    A = np.clip(2 * (sig_BC**2 - sig_RS**2) * np.log(A),0,1)
    A = sig_BC * sig_RS * np.sqrt((mu_BC-mu_RS)**2 + A)
    # 2) in square brackets terms
    B = 1/(sig_BC**2 - sig_RS**2)
    C = mu_RS*sig_BC**2 - mu_BC*sig_RS**2
    m,p = np.array([B * (C + mp * A) for mp in [-1,+1]])
    p[p<m] = np.inf
    return m,p
  
  
  def wyvren_pred1D(self,Z,bands,bands_err=None):
    """
    1D RS definition: hard single color cut
    wyvren_pred1D(self,Z,bands,bands_err=None)
    """
    m,p = self.wyvren_bounds(Z) # lone color RS lower & upper bounds
    cols = -np.diff(bands)
    if bands_err is None:
      # boolean selection
      return (m < cols) * (cols < p)
    else:
      col_err = _band_err_to_col_err(bands_err)
      return _Gaussian_interval(cols,col_err,m,p)
  
  
  ######################################################################
  # other tools 
  
  def get_Pz(self,bands,bands_err=None,interpolate=1):
    """
    get_Pz(self,bands,bands_err=None,interpolate=1)
    For each component, find probability galaxy belongs at redshift,
    i.e. P_RS(Z), P_BC(Z), and so forth, for each galaxy. 
    Returns (Z,Pz), with Pz.shape = (len(Z),N_fit,N_gal)
    
    interpolate=1 : multiplicitive factor for Z sampling frequency, 
                    e.g. 2 gives twice as many evaluation points
    
    this is somewhat like a photo-z estimation, with the assumption
    that the galaxy either belongs to the RS or BC; it will skew
    more if the galaxy is further from the center of the RS or BC.
    """
    Z = self._Z_bins
    Z = np.linspace(Z[0],Z[-1],len(Z)*int(interpolate))
    report = np.zeros((len(Z),self.N_fit,len(bands)))
    for ii in range(len(Z)):
      report[ii] = self._get_logL_slice(Z[ii],bands,bands_err)
    return Z,report
  
  
  def get_photoZ(self,bands,bands_err=None,interpolate=5):
    """
    estimate redshift, assuming the galaxy is a member belongs at 
    the central location of one of the components
    
    returns z_max_vals with shape (K,N_gals), 
            where K = number of components in the dragon's model
    """
    # read in metadata from dragon and inputs
    K = self.N_fit
    N_gals = bands.shape[0]
    # calculate log P(Z) from colors and dragon model
    zv,Pz = self.get_Pz(bands,bands_err,interpolate)
    # Pvn = np.exp(Pz)/np.max(np.exp(Pz),axis=(0,1))[np.newaxis,np.newaxis,:]
    mx = np.max(np.exp(Pz),axis=0) # max likelihood for a given K
    z_max_vals = np.array([[zv[np.argwhere(np.exp(Pz[:,jj,ii]) == mx[jj,ii])[0][0]] \
                            for ii in range(N_gals)] for jj in range(K)])
    return z_max_vals
  
  
  def normalize_colors(self,fname_in,fname_out=None):
    """
    normalize_colors(self,fname,fname_out=None)
    return normalized data, such that the RS follows Normal(0,1)
    (break degeneracy of solutions by keep bands[:,0] unchanged)
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # read in files
    print("Reading in",fname_in)
    with h5py.File(fname_in,'r') as ds:
      # read in redshift, bands, errors
      Z,bands_old = [ds[key][:] for key in ['Z','bands']]
      errs_old = ds['bands_err'][:] if 'bands_err' in ds.keys() \
                                    else np.zeros_like(bands_old)
      Z_err = ds['Z_err'][:] if 'Z_err' in ds.keys() \
                             else np.zeros_like(Z)
      
    # read in red dragon; grab RS properties over redshift
    print("Calculating mu_RS") 
    mu_RS = self.get_mu(Z)[:,0] # RS means
    print("Calculating Sigma_RS") 
    Sigma_RS = self.get_Sigma(Z)[:,0] # RS covariance matrices
    sig_RS = np.sqrt(np.diagonal(Sigma_RS,0,-1,-2)) # RS std dev
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # calculate shifts 
    print("Shifting data")  
    bands_new = np.zeros_like(bands_old)
    bands_new[:,0] = bands_old[:,0] # break degeneracies by this fiat
    errs_new = np.zeros_like(errs_old)
    for ii in range(self.N_cols): 
      bands_new[:,ii+1] = bands_new[:,ii] + (bands_old[:,ii+1] \
                        - bands_old[:,ii] + mu_RS[:,ii]) / sig_RS[:,ii]
      errs_new[:,ii] = errs_old[:,ii] / sig_RS[:,ii]
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # save output
    print("Saving output")
    
    if fname_out is None:
      # set up default filename
      parts = fname_in.split('.')
      parts[-2] += '_norm'
      fname_out = '.'.join(parts)
    
    with h5py.File(fname_out,'w') as ds:
      # save normalized bands, &c. 
      for key,vals in zip(['Z','bands','Z_err','bands_err'],
                          [Z, bands_new, Z_err, errs_new]):
        ds.create_dataset(key,data=vals)
      ds.attrs['Note'] = 'colors normalized by RS'
    
    print("Saved",fname_out)
  
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # mirror functions from below for ease of plotting
  
  def plot_fits(self,**kwargs):
    plot_fits(self,**kwargs)
  
  def plot_scattcorr(self,**kwargs):
    plot_scattcorr(self,**kwargs)
  
  def plot_WCR(self,**kwargs):
    plot_WCR(self,**kwargs)
  
  def plot_cZ(self,**kwargs):
    plot_cZ(self,**kwargs)
  
  pass # end of `dragon` class




########################################################################
# fitting functions 
########################################################################


def fit_truth(ds,KLLR_kw=None,Z_lim=None,N_Z=1000,sig_max=5,rho_0=.9,
              fname_out='output/fit.h5'):
  """
  use truth labels `truth` to do KLLR first pass on mean and scatter
  
  ds : input h5py dataset
  KLLR_kw=None : KLLR kernel width; if None, set to fifth of width
  Z_lim=None
  N_Z=1000
  sig_max=5
  rho_0=.9 : default correlations for off-diagonals of covariance matrix
  fname_out='output/fit.h5' : output filename 
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # read in data from dataset ds
  bands = ds['bands'][:]
  N_gals,N_bands = np.shape(bands)
  D = N_cols = N_bands - 1 # number of colors in mixture
  cols = -np.diff(bands,axis=1) # color vector 
  Z = ds['Z'][:]
  if Z_lim is None:
    Z_lim = np.quantile(Z,[0,1])
  # set up Z-bins
  dZ = np.squeeze(np.diff(Z_lim)) / (N_Z - 1)
  Z_bins = np.linspace(Z_lim[0]-dZ/2.,Z_lim[1]+dZ/2.,N_Z+1)
  fname = ds.filename
  # read in errors, if using
  use_errors = 'bands_err' in ds.keys()
  if use_errors:
    bands_err = ds['bands_err'][:]
    cols_err = _band_err_to_col_err(bands_err)
  else:
    cols_err = None
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # fit with KLLR in each color
  truth_set = set(ds['truth'])
  N_fit = K = len(truth_set)
  N_pars = N_fit * (N_cols**2 + N_cols + 1) - 1 # number of paramters
  lm = kllr_model(kernel_width=KLLR_kw)
  w = np.zeros((N_Z,K))
  mu = np.zeros((N_Z,K,D))
  Sigma = np.zeros((N_Z,K,D,D))
  for ii,val in enumerate(truth_set):
    # for each population,
    mask = ds['truth'][:] == val
    for jj in range(D):
      # for each color, 
      # measure mean and scatter of population
      col = cols[mask,jj]
      err = None if cols_err is None else cols_err[mask,jj]
      # run KLLR
      with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        xl,yl,ic,sl,sc,sk,kt = lm.fit(Z[mask], col, xrange=Z_lim, 
                                      bins=N_Z, nBootstrap=1, y_err=err)
      # store parameters 
      w[:,ii] = get_pseudocount(Z[mask], xl, KLLR_kw) # count
      mu[:,ii,jj] = yl # mean 
      Sigma[:,ii,jj,jj] = sc**2 # scatter
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # tidy up values
  N_tot = np.sum(w,axis=1) # total counts in each bin
  w /= N_tot[:,np.newaxis] # make weights instead of counts
  for ii in range(D):
    for jj in range(ii+1,D):
      Sij = np.sqrt(Sigma[:,:,ii,ii] * Sigma[:,:,jj,jj])
      Sigma[:,:,ii,jj] = Sigma[:,:,jj,ii] = rho_0 * Sij
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save weight, mean, and scatter
  with h5py.File(fname_out,'w') as ds:
    for key,value in zip(['w','mu','Sigma','Z_bins'],
                        [ w , mu , Sigma , Z_bins ]):
      ds.create_dataset(key,data=value)
    for key,value in zip(['N_fit','N_cols','use_errors','N_used','N_gals',
                          'N_bands','fname','N_pars','trained'],
                         [ N_fit , N_cols , use_errors , N_tot  , N_gals , 
                           N_bands , fname , N_pars , False ]):
      ds.attrs[key] = value if value is not None else 'None'
  print(fname_out,'created!')


def fit_slices(ds,Z_bins,N_fit=2,use_errors=False,use_initial=None,
               fname_out='output/fit.h5',printing=True,plotting=False,
               lim_weight=.001,sig_max=5,timing=True,N_Z=10**4):
  """
  fit_slices is the main workhorse in creating Red Dragon DNA, 
  i.e. the initial stab at parameterizing the populations on a slice.
  
  inputs: (ds,Z_bins,N_fit=2,use_errors=False,use_initial=None,
           fname_out='output/fit.h5',printing=True,plotting=False,
           lim_weight=.001,sig_max=4,timing=True,N_Z=10**4):
  
  ds : opened h5py dataset to read in from. must have 'bands' and 'Z';
       may also have 'bands_err' or 'Z_err' to include errors in fits.
  Z_bins : Z bins in which to fit parameters
  N_fit=2 : number of Gaussian mixture components to use in fit
  use_errors=False : if true, use pyGMMis; else use sklearn
  use_initial=None : RD file to use as initial conditions
  fname_out='output/fit.h5' : output filename
  printing=True : toggle print statements to show progress
  plotting=False : toggle plotting fit (check whether good fit found)
  lim_weight=.001 : minimum weight allowed to be included; exclude bins
                    where one of the components has near null weight
  sig_max=5 : maximum std dev away from mean color allowed per slice
              helps to ignore photometric outliers and get better fits.
  timing=True : calculate and print timing information
  N_Z=10**4 : maximum number of galaxies to input to fit
  """
  if timing:
    old_time = curr_time = start = time()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # read in data from dataset ds
  Z_mids = (Z_bins[1:] + Z_bins[:-1])/2.
  bands = ds['bands'][:]
  _,N_bands = np.shape(bands)
  N_cols = N_bands - 1 # number of colors in mixture
  N_pars = N_fit * (N_cols**2 + N_cols + 1) - 1 # number of paramters
  X = -np.diff(bands,axis=1)
  Z = ds['Z'][:]
  fname = ds.filename
  # read in errors, if using
  if use_errors:
    Z_err = ds['Z_err'][:]
    bands_err = ds['bands_err'][:]
    col_err = _band_err_to_col_err(bands_err)
    covars = _band_err_to_covar(bands_err)
  else:
    Z_err = np.zeros_like(Z)
    bands_err = np.zeros_like(bands)
    col_err = np.zeros_like(X)
  # read in baseline, if using
  if use_initial is not None:
    if use_initial.__class__ is str:
      rd = dragon(use_initial) # set up dragon for initial guess 
      if 0: # good place to check fits; soften input
        # plot_fits(rd)
        # plot_WMS(rd)
        # plot_WCR(rd)
        pass
    elif use_initial.__class__ is dragon:
      rd = use_initial # use input dragon
    else:
      print(f"ERROR: Invalid input class ({use_initial.__class__})")
      return
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # set up report variables
  N_bins = len(Z_bins) - 1
  exclusions,N_gals,N_used,N_excluded,BIC,times = np.zeros((6,N_bins))
  R_vals,C_vals = np.zeros((2,N_bins,N_fit))
  w = np.zeros((N_bins,N_fit))
  mu = np.zeros((N_bins,N_fit,N_cols))
  Sigma = np.zeros((N_bins,N_fit,N_cols,N_cols))
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # iterate through redshift bins
  for ii in range(N_bins):
    ####################################################################
    # calculate timing information
    if timing:
      curr_time = time()
      elapsed = time() - start
      if elapsed > 3:
        # set up time since last
        dt = curr_time - old_time
        if dt < 90:
          a = f"{dt:.1f} s"
        else:
          a = f"{dt/60.:.1f} min"
        # set up total elapsed time
        if elapsed < 90:
          b = f" ({elapsed:.0f} s total)"
        else:
          b = f" ({elapsed/60.:.0f} min total)"
        print("dt: " + a + b)
      old_time = curr_time
    
    ####################################################################
    # Redshift cuts
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # hard cut redshift slice for starters
    mask_ii = (Z_bins[ii] <= Z) * (Z < Z_bins[ii+1])
    # count of galaxies with their means within the bounds
    N_gals[ii] = len(mask_ii[mask_ii])
    lowcount = (N_gals[ii] < N_fit * 10) + (N_gals[ii] < N_pars)
    warn = " (low number count!)" if lowcount else ""
    print(f"{ii+1}/{N_bins}: Z|[{Z_bins[ii]:0.4g},{Z_bins[ii+1]:0.4g})" \
          f" -> {N_gals[ii]:g} galaxies" + warn)
    if N_gals[ii] < 2 * N_fit:
      # need >=2 points per component for a scatter measurement
      print("Skipping, since N < 2 * N_fit.")
      exclusions[ii] = 1
      continue
    
    """
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # toss outlier galaxies 
    if sig_max > 0: # if summed N_sigma > sig_max, exclude galaxies
      mn,st = [f(X[mask_ii],axis=0) for f in [np.mean,np.std]]
      sig = (X[mask_ii] - mn)/np.sqrt(st**2 + col_err[mask_ii]**2)
      sig_tot = np.sqrt(np.sum(sig**2,axis=1))
      mask_sig = sig_tot < sig_max # non-outlier galaxies
      N_exclude = len(mask_sig[~mask_sig])
      mask_ii[mask_ii] = mask_sig
      print(f"Number of outliers excluded: {N_exclude}/{N_gals[ii]}")
    """
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # select via probabilistic cut
    if use_errors and np.all(Z_err>0):
      # use probabilistic redshift cut
      weight = _Gaussian_interval(Z,Z_err,Z_bins[ii],Z_bins[ii+1])
    else: # hard redshift cut
      weight = np.copy(mask_ii)
    P = weight/np.sum(weight)
    # if N_Z < len(Z): # TODO: consider making switch to prevent oversampling!
    N_sample = min(N_Z,N_gals[ii])
    if N_sample == -1:
      N_sample = N_gals[ii]
    mask_ii = np.random.choice(len(Z),size=int(N_sample),replace=True,p=P)
    
    ####################################################################
    # remove spurious points in color space
    
    if sig_max > 0:
      # if summed sigma > sig_max, exclude
      mn,st = [f(X[mask_ii],axis=0) for f in [np.mean,np.std]]
      sig = (X[mask_ii] - mn)/np.sqrt(st**2 + col_err[mask_ii]**2)
      sig_tot = np.sqrt(np.sum(sig**2,axis=1)/N_cols)
      mask_sig = sig_tot < sig_max # non-outlier galaxies
      N_excluded[ii] = len(mask_sig[~mask_sig])
      if N_excluded[ii] > 0:
        f_exclude = N_excluded[ii]/len(mask_sig)
        print(f"Fraction excluded as outliers: {f_exclude*100:0.3g}%")
      mask_ii = mask_ii[mask_sig]
    
    N_used[ii] = len(mask_ii) # list of indices, not boolean mask
    
    
    ####################################################################
    # set up initial weights, if using
    
    if use_initial is not None:
      weights_ii = rd.get_weights(Z_mids[ii])
      means_ii = rd.get_means(Z_mids[ii])
      covars_ii = rd.get_covars(Z_mids[ii])
      precs_ii = np.linalg.inv(covars_ii)
    elif False and (ii > 0) and (exclusions[ii-1] == 0):
      # DEACTIVATED for now
      # use previous values if they fit well
      weights_ii = w[ii-1]
      means_ii = mu[ii-1]
      covars_ii = Sigma[ii-1]
      precs_ii = np.linalg.inv(covars_ii)
      pass
    else: # reset to None if no good previous values exist
      weights_ii,means_ii,covars_ii,precs_ii = None, None, None, None
    
    ####################################################################
    # measure fit
    if use_errors: # use pygmmis
      covar = covars[mask_ii]
      gmm = pygmmis.GMM(K=N_fit, D=N_cols)
      if use_initial is not None:
        gmm.amp = weights_ii
        gmm.mean = means_ii
        gmm.covar = covars_ii
        init = 'none'
      else:
        init = 'kmeans'
      try:
        logL,U = pygmmis.fit(gmm,X[mask_ii],covar,init_method=init)#,split_n_merge=1)
      except np.linalg.LinAlgError as e:
        print(e)
        exclusions[ii] = 1
        print("Skipping bin.")
        continue
      BIC[ii] = N_pars * np.log(N_sample) - 2 * logL # Bayesian info. crit.
      weights_ii = gmm.amp
      means_ii = gmm.mean
      Sigma_ii = gmm.covar
      Ps = [gmm.logL_k(jj,X[mask_ii],covar) for jj in range(N_fit)]
      pred = np.argmax(Ps,axis=0)
    else: # use sklearn
      gmm = GM(N_fit, weights_init=weights_ii, means_init=means_ii, precisions_init=precs_ii)
      fit = gmm.fit(X[mask_ii]) 
      BIC[ii] = fit.bic(X[mask_ii]) # Bayesian information criterion
      weights_ii = fit.weights_
      means_ii = fit.means_
      Sigma_ii = fit.covariances_
      precs_ii = fit.precisions_
      pred = fit.predict(X[mask_ii])
    
    ####################################################################
    # fix sorting for first pass save of data
    # 1) Calculate color scatter for this data slice
    #    Is this component more or less scattered than the others?
    S_all = np.std(X[mask_ii],axis=0) # std dev of this data slice
    S_fit = [np.std(X[mask_ii][pred==jj],axis=0) for jj in range(N_fit)]
    R = S_fit / S_all[np.newaxis,:] # relative scatters of each color
    R = np.mean(R,axis=1) # mean of relative color scatters
    # 2) Calculate color shift for this data slice
    #    Is this component generally bluer or redder than the others? 
    M_all = np.median(X[mask_ii],axis=0) # median of this data slice
    M_fit = np.array([np.median(X[mask_ii][pred==jj],axis=0) \
                      for jj in range(N_fit)])
    M_diff = M_fit - M_all[np.newaxis,:] # color shift from median
    C = M_diff / S_all[np.newaxis,:] # how many sigma scatt. from median
    C = np.mean(C,axis=1) # mean sigma shift from median across all cols
    # 3) create summary statistic to detangle components
    factor = -.5 # for K>2 may be better w/ e.g. -1
    sort = np.flip(np.argsort(C + factor * R))
    R_vals[ii] = R[sort]
    C_vals[ii] = C[sort]
    
    ####################################################################
    # check whether it was a bad fit; mark if so
    if np.any(weights_ii*(1-weights_ii) < lim_weight*(1-lim_weight)):
      exclusions[ii] = 1
    
    ####################################################################
    # save redshift slice
    w[ii] = [weights_ii[sort][jj] for jj in range(N_fit)]
    mu[ii] = [means_ii[sort][jj] for jj in range(N_fit)]
    Sigma[ii] = [Sigma_ii[sort][jj] for jj in range(N_fit)]
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot fit of this slice
    if plotting: 
      re_pred = np.copy(pred)
      for jj in range(N_fit):
        re_pred[pred==jj] = sort[jj]
      plt.subplot(121)
      plt.scatter(X[mask_ii,0],X[mask_ii,1],3,alpha=.25,lw=0,c=re_pred)
      plt.subplot(122)
      if N_cols >=4:
        plt.scatter(X[mask_ii,2],X[mask_ii,3],3,alpha=.25,lw=0,c=re_pred)
      else:
        for jj in range(N_fit):
          _ = plt.hist(X[mask_ii,2][re_pred==jj],100,histtype='step')
      plt.show()
    
    if timing:
      times[ii] = time() - old_time
  
  ######################################################################
  # save output to file
  with h5py.File(fname_out,'w') as ds:
    for key,data in zip(['w','mu','Sigma','Z_bins'],
                        [ w , mu , Sigma , Z_bins ]):
      ds.create_dataset(key,data=data)
    for key,data in zip(['exclusions','N_fit','N_cols','fname',
                         'use_initial','use_errors','lim_weight','N_Z',
                         'trained','N_gals','N_used','BIC','times',
                         'N_excluded','N_pars','R_vals','C_vals'],
                        [ exclusions , N_fit , N_cols , fname ,
                          use_initial , use_errors , lim_weight , N_Z ,
                          False   , N_gals , N_used , BIC , times ,
                          N_excluded , N_pars , R_vals , C_vals]):
      ds.attrs[key] = data if data is not None else 'None'
  return fname_out


def fit_file(fname,N_fit=2,fname_out=fname_default,N_max=2*10**4,
             use_initial=None,Z_min=None,Z_max=None,lim_weight=.01,
             printing=True,use_errors=None,dZ_0=.05,dZ_1=.025,pause=0,
             use_errors_initial=False):
  """
  fname : input dataset to fit
  N_fit=2 : number of Gaussian components to model galaxy photometrics with
  fname_out='output/fit.h5' : output filename containing fit
  N_max=2e4 : maximum number of points to use at a given redshift slice
  use_initial=None : filename for initial parameter guess
  Z_min,Z_max=None,None : range of redshifts to evaluate (default spans)
  lim_weight=.01 : minimum component weight to be included in interpolation model
  printing=True : print progress
  use_errors=None : use pyGMMis (default is to use errors if given) vs sklearn
  dZ_0,dZ_1=.025,.01 : redshift bin sizes for sparse and detailed fits
  pause=0 : plot sparse fit mid-calculation
  use_errors_initial=False : use sklearn by default for first pass
  """
  assert(N_fit > 1) # otherwise you can't select RS
  # multi-step file analyzer
  ds = h5py.File(fname,'r')
  Z = ds['Z'][:]
  has_errs = 'bands_err' in ds.keys() # else no errors available
  if use_errors is not None:
    has_errs = use_errors
  
  if printing:
    print("File read in successfully.")
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up bins
  if Z_min is None:
    Z_min = min(Z)
  if Z_max is None:
    Z_max = max(Z)
  # min/max better than quantiles, since it catches all extremes
  # quantiles good if user-defined, but depends on density of dataset
  
  if use_initial is None:
    # 1) simple wide-bin sklearn fit as base model
    if printing:
      print("Sparse fitting...")
    # set up bins
    N_bins = np.round((Z_max - Z_min)/dZ_0).astype(int) # bin count
    Z_bins = np.linspace(Z_min,Z_max,N_bins+1) # analysis bins
    # calculate temporary output filename
    split = fname_out.split('/')
    split[-1] = 'tmp_' + split[-1]
    fname_tmp = "/".join(split)
    # run model
    out = fit_slices(ds,Z_bins,N_fit,use_errors=use_errors_initial,
                     fname_out=fname_tmp,use_initial=use_initial,
                     printing=printing,lim_weight=lim_weight,N_Z=N_max)
    if True: # sort output fit
      rd = dragon(out)
      rd.superior_sort()
      rd.set_fits()
      rd.save_training()
    if printing:
      print("Saved",out)
    if pause:
      plot_fits(out)
  else:
    # use default from input arguments
    fname_tmp = use_initial
    if printing:
       print("Using input file as default:",fname_tmp)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # 2) finer-binned fit, incl errors if available
  if printing:
    print("Finer fitting...")
  # set up bins
  N_bins = np.round((Z_max - Z_min)/dZ_1).astype(int) # bin count
  Z_bins = np.linspace(Z_min,Z_max,N_bins+1) # analysis bins
  out = fit_slices(ds,Z_bins,N_fit,use_errors=has_errs,fname_out=fname_out,
                   use_initial=fname_tmp,printing=printing,
                   lim_weight=lim_weight,N_Z=N_max)
  if printing:
    print("Saved",out)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # 3) check for exclusions or misplaced values
  rd = dragon(fname_out)
  return rd



########################################################################
# tool to measure and record color uncertainties for future use

def measure_color_uncertainty(f_in,f_out=None,overwrite=False):
  # set up output file
  if f_out is None:
    f_out = f_in[:-3] + "_sigcol.h5"
  if not overwrite:
    if os.path.exists(f_out):
      return f_out
  # read in data
  with h5py.File(f_in,'r') as f:
    keys = ['Z','Z_err','bands','bands_err']
    Z,Z_err,bands,bands_err = [f[k][:] for k in keys]
    cols_err = _band_err_to_col_err(bands_err)
    N_cols = np.shape(cols_err)[1]
  # analyze 
  bins = np.linspace(min(Z),max(Z),11)
  mids = (bins[:-1] + bins[1:]) / 2.
  qv = .5 + np.array([-s2,-s1,0,s1,s2]) / 2.
  qs = np.zeros((len(mids),N_cols,len(qv)))
  for ii in range(len(mids)):
    mask_ii = (bins[ii] < Z) * (Z < bins[ii+1])
    if len(Z[mask_ii]) < 1:
      continue
    else:
      for jj in range(N_cols):
         qs[ii,jj],_ = boot_fun(np.quantile,cols_err[mask_ii,jj],q=qv)
  # save output
  with h5py.File(f_out,'w') as ds:
    ds.create_dataset('Z_bins',data=bins)
    ds.create_dataset('Z_mids',data=mids)
    ds.create_dataset('col_errs',data=qs)
    ds.create_dataset('qv',data=qv)
    ds.attrs['Note'] = "col_errs shape: (len(Z_mids),N_cols,len(qv))"
    ds.attrs['f_in'] = f_in
  return f_out




########################################################################
# bootstrapping function to automate some fitting 

def run_bootstrap(f_in,ii0=0,N_boot=50):
  """
  Given an input red dragon, e.g. `bla.h5`, use directory `bla/` and 
  do bootstrap runs as `bla/run003.h5`
  """
  # set up output template from input dragon
  if f_in.__class__ is dragon: # if input is dragon, pull fname
    f_in = f_in.fname_dragon
  assert(f_in[-3:]=='.h5')
  f_base = f_in[:-3]
  f_tmp = f_base + "/run%03d.h5"
  # check that there's a folder to catch the dragons to be created
  if not os.path.isdir(f_base):
    os.mkdir(f_base)
    if not os.path.isdir(f_base):
      mssg = f'Could not create directory {f_base}'
      raise FileNotFoundError(mssg)
    else:
      print("Successfully created directory",f_base)
  # pull relevant data from base dragon
  rd0 = to_rd(f_in)
  K = rd0.K
  Z_min = rd0._Z_bins[0]
  Z_max = rd0._Z_bins[-1]
  dZ = rd0._Z_bins[1] - rd0._Z_bins[0]
  fname = rd0.fname_data
  # iterate through each bootstrap realization
  for ii in range(ii0,N_boot):
    print(f"\nRunning index #{ii} (of {N_boot})")
    f_out = f_tmp % ii
    rd = fit_file(fname,K,fname_out=f_out,Z_min=Z_min,Z_max=Z_max,
                  dZ_1=dZ,use_initial=f_in)
    rd.label_primary = rd0.label_primary
    rd.label_colors = rd0.label_colors
    rd.superior_sort()
    rd.set_fits()
    rd.save_training()
    print(f"{f_out} saved.\n")
  # return base directory of newly created dragons 
  return f_base


########################################################################
# combine multiple dragons from bootstrapping 

def merge_dragons(fdir,f_out=None):
  """
  Given a directory, find all red dragon files within and merge
  all bootstrap realizations of dragons running on the same data.
  Assumes similar shape, already sorted between components properly
  
  fdir : directory to search for red dragon files
  f_out : output filename to save merged dragon
  """
  # set up output filename
  if f_out is None:
    if fdir[-1] == '/': # strip ending slash if needed.
      fdir = fdir[:-1]
    f_out = fdir + '_merged.h5'
  # check whether that already exists
  if os.path.exists(f_out):
    warnings.warn(f"{f_out} already exists\n")
  print('Saving to',f_out)
  # set up list of files to merge
  files = sorted(glob(fdir + "/*.h5"))
  N_boot = len(files)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in first dragon for some defaults
  rd0 = dragon(files[0])
  N_fit = K = rd0.N_fit
  N_cols = D = rd0.N_cols
  Z_mids = rd0._Z_mids
  L = len(Z_mids) # number of redshift bins
  # set up fields to pull
  w = np.zeros((N_boot,L,N_fit))
  mu = np.zeros((N_boot,L,N_fit,N_cols))
  Sigma = np.zeros((N_boot,L,N_fit,N_cols,N_cols))
  BIC = np.zeros((N_boot,L))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in all dragons to merge data
  for ii,f in enumerate(files):
    rd = dragon(f) # TODO: set up some check to ensure it's a dragon!!
    w[ii] = rd._w
    mu[ii] = rd._mu
    Sigma[ii] = rd._Sigma
    BIC[ii] = rd.BIC
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # test for outliers using deviation from weight trend
  dw_vals = np.nanmean(np.abs(w-np.median(w,axis=0)[None,...]),axis=(1,2))
  sigmas = (dw_vals - np.median(dw_vals))/np.nanstd(dw_vals)
  if np.any(sigmas>3):
    which = np.arange(N_boot)[sigmas>3]
    print("outlier files:",which)
    for ii in which:
      print(' *',files[ii])
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # save output 
  rd0._w,rd0._w_err = [f(w,axis=0) for f in nanmnst]
  rd0._mu,rd0._mu_err = [f(mu,axis=0) for f in nanmnst]
  # rd0._Sigma = np.nanmean(Sigma,axis=0)
  # use more nuanced approach for Sigma
  lg_var = np.log(np.diagonal(Sigma,0,-1,-2)) # log of variances
  sigs,sigs_err = [np.sqrt(np.exp(f(lg_var,axis=0))) for f in nanmnst]
  corr = _corr_from_Sigma(Sigma) # grab all correlations
  corr_mn,corr_err = [f(corr,axis=0) for f in nanmnst]
  I = np.identity(N_cols) # to turn scatter into matrix
  for ii in range(N_cols):
    corr_err[:,:,ii,ii] = 1 # set diagonal to unity (from ~0)
  D = I[None,None,...]*sigs[...,None] # diagonal matrix to multiply by
  D_err = I[None,None,...]*sigs_err[...,None] # "
  rd0._Sigma = D@corr_mn@D # store means in proper format
  rd0._Sigma_err = D_err@corr_err@D_err # store uncertainties properly
  
  rd0.set_fits()
  rd0.save_training(f_out)
  with h5py.File(f_out,'a') as ds:
    ds.attrs['fname'] = rd0.fname_data
    ds.attrs['fdir'] = fdir
    ds.attrs['N_boot'] = N_boot
    ds.attrs['BIC'] = np.nanmean(BIC,axis=0)
    ds.attrs['BIC_err'] = np.nanstd(BIC,axis=0)
  
  return f_out


def plot_cf_boots(fdir,which=[0,1],offset=True):
  """
  compare w/mu/sigma for several dragons of the same bootstrapping
  given file path `fdir`, find all dragons within, 
  assert they all have the same dimensionality D, 
  and plot w, mu, scatter for all components 
  
  `offset`=True : whether or not to offset points slightly 
                  in order to better track & distinguish them. 
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # function for switch
  def check(num):
    " see whether or not to plot given aspect [0,1,2] "
    return (which is num) or (hasattr(which,'__len__') and (num in which))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up list of files to merge
  files = sorted(glob(fdir + "/*.h5"))
  N_boot = len(files)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in first dragon for some defaults
  rd0 = dragon(files[0])
  N_fit = K = rd0.N_fit
  N_cols = D = rd0.N_cols
  Z_mids = rd0._Z_mids
  dZ = np.median(np.diff(Z_mids))
  L = len(Z_mids) # number of redshift bins
  label_primary = rd0.label_primary
  label_colors = rd0.label_colors
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up fields to pull
  w = np.zeros((N_boot,L,N_fit))
  mu = np.zeros((N_boot,L,N_fit,N_cols))
  Sigma = np.zeros((N_boot,L,N_fit,N_cols,N_cols))
  # read in all dragons to merge data
  for ii,f in enumerate(files):
    rd = dragon(f) # TODO: set up some check to ensure it's a dragon!!
    w[ii] = rd._w
    mu[ii] = rd._mu
    Sigma[ii] = rd._Sigma
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot each field
  if offset:
    shift = np.random.normal(0,dZ/6,N_boot)
  else:
    shift = np.zeros(N_boot)
  
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
  if check(0): # plot weights 
    plt.figure(1)
    for ii in range(N_boot): # for each file
      for jj in range(N_fit): # for each component
        col = mpl_colors_RF[jj] # set up color for that component 
        plt.scatter(Z_mids+shift[ii],w[ii,:,jj],3,
                    color=col,alpha=.75,lw=0)
  
    plt.xlabel(label_primary)
    plt.ylabel('Weight')
    ylim = plt.ylim()
    plt.ylim(max(0,ylim[0]), min(1,ylim[1]))
  
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
  if check(1): # plot means 
    for kk in range(N_cols): # for each color
      plt.figure(2+kk)
      for ii in range(N_boot): # for each file
        for jj in range(N_fit): # for each component
          col = mpl_colors_RF[jj] # set up color for that component 
          plt.scatter(Z_mids+shift[ii],mu[ii,:,jj,kk],3,
                      color=col,alpha=.75,lw=0)
      plt.xlabel(label_primary)
      plt.ylabel(r'$\langle ' + label_colors[kk] + r' \rangle$')
  
  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #
  if check(2): # plot scatters
    sigs = np.sqrt(np.diagonal(Sigma,0,-1,-2))
    for kk in range(N_cols): # for each color
      plt.figure(2+N_cols+kk)
      for ii in range(N_boot): # for each file
        for jj in range(N_fit): # for each component
          col = mpl_colors_RF[jj] # set up color for that component 
          plt.scatter(Z_mids+shift[ii],sigs[ii,:,jj,kk],3,
                      color=col,alpha=.75,lw=0)
      plt.xlabel(label_primary)
      plt.ylabel(r'$\sigma_{' + label_colors[kk] + r'}$')
  
  plt.show()



def find_outlier_bootstraps(fdir):
  " detect bootstrap runs which behave oddly "
  # use sklearn GM to make preliminary groups
  # from the best-grouped redshift bins, 
  # detect whether any dragons are strong outliers
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up list of files to merge
  files = np.array(sorted(glob(fdir + "/*.h5")))
  rds = [dragon(f) for f in files]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # make mama array for sklearn GM
  all_flat = np.array([rd.get_flat_pars() for rd in rds])
  L,N,K,N_pars = all_flat.shape
  all_X = [np.reshape(all_flat[:,ii],(L*K,N_pars)) for ii in range(N)]
  # figure out which ones are well-sorted
  mins = np.zeros(N)
  all_pred = np.array([GM(K).fit_predict(X_ii) for X_ii in all_X])
  mnmn = lambda x: min([len(x[x==ii]) for ii in range(K)])
  mins = np.array([mnmn(pred) for pred in all_pred])
  # find index of most stable peak
  if len(set(mins)) == 1:
    print("All indices equally stable.")
    idx_peak = 0
  else:
    idx_peak = find_peak(mins)
    Z_peak = rds[0]._Z_mids[idx_peak]
    print(f'peak stability at Z_mids[{idx_peak}]={Z_peak:0.10g}')
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # define function to analyze redshift bin for stability
  def analyze_index(idx):
    Z_idx = rds[0]._Z_mids[idx]
    # print(f'analyzing Z_mids[{idx}]={Z_idx:0.10g}')
    pred_idx = all_pred[idx].reshape((L,K)) # order predictions at idx
    mode = stats.mode(pred_idx).mode # determine most common grouping
    mask_outliers = np.any(pred_idx != mode,axis=1) # find deviant files
    return np.arange(L)[mask_outliers]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # start with analyzing peak index
  bad_indices = analyze_index(idx_peak)
  # analyze indices to the left
  for idx in np.flip(np.arange(idx_peak)):
    bad_indices = np.concatenate([bad_indices,analyze_index(idx)])
  # analyze indices to the right
  for idx in np.arange(idx_peak+1,N):
    bad_indices = np.concatenate([bad_indices,analyze_index(idx)])
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # TODO: analyze which indices pop up most often, perhaps weighted by order, so you prioritize misfits close to the peak. 
  unq = np.unique(bad_indices,return_counts=True)
  sort = np.flip(np.argsort(unq[1])) # sort by most problematic sets
  problems = unq[0][sort] # prioritized set of problems
  mn,st = [f(unq[1][sort]) for f in mnst]
  if st == 0:
    st = 1
  rs = (unq[1][sort] - mn)/st
  print(problems[rs>1])
  return bad_indices


def meta_sort(fdir):
  """
  given a hoard of dragons, use net results to find optimal sorting
  meant to be the final say of all sorters, but ... that's not likely.
  
  Outline: 
  1. Get rank from each
  2. Calculate optimal ranking from that set of ranks
  3. Use that optimal ranking to sort all dragons with
  """
  ######################################################################
  # read in files and set up ranks
  files = sorted(glob(fdir + "/*.h5"))
  rds = [dragon(f) for f in files]
  all_ranks = np.array([rd.get_rank() for rd in rds])
  N_boot,N_bins,N_pars = all_ranks.shape
  ranks_mean = 10**np.nanmean(np.log10(all_ranks),axis=0)
  ranks_smooth = np.transpose([10**smooth(np.log10(ranks_mean[:,ii])) \
                               for ii in range(N_pars)])
  ######################################################################
  # set in itial sorting
  # find cleanest parameter and best starting location
  max_rank = np.max(ranks_smooth,axis=1) # highest score across pars
  idx_start = np.min(np.argmax(max_rank)) # best starting index
  idx_best_par = np.argmax(ranks_smooth,axis=1)[idx_start] # best par
  # find sorting of initial dragon
  vals = rds[0].get_flat_pars()[idx_start,:,idx_best_par]
  sorting = vals.argsort().argsort() # sorting of that best parameter
  ######################################################################
  # iterate through dragons, sorting by the smoothed ranking
  for rd in rds:
    # align sorting
    vals = rd.get_flat_pars()[idx_start,:,idx_best_par]
    sort = vals.argsort()[sorting]
    for ii in range(rd.N):
      rd.sort(ii,sort)
    # sort using globally smoothed ranking
    rd.sort_from_rank(ranks_smooth)
    rd.set_fits()
    rd.save_training()


def dir_sort(fdir,mask,order):
  """
  for each dragon in the input directory `fdir`, 
  use string `mask` to find which indices to change, 
  then use `order` to sort, like rd.sort(indices,order).
  
  `mask` is a string, evaluated for each dragon `r`. 
  example masks (where `r` is current dragon being sorted): 
   * "(r._Z_mids < 3) * (r._w[:,1] < r._w[:,0])"
   * "(r._Z_mids < .5) * (r._mu[:,1,2] > r._mu[:,0,2])"
   * "r._Z_mids == r._Z_mids[rd.idx_where_Z(2)]"
  """
  files = sorted(glob(fdir + "/*.h5"))
  # check directory has files
  if len(files) < 1:
    message = "No files found in "+fdir
    raise FileNotFoundError(message)
  # check sorting array is valid, and won't ruin the world
  if len(set(order)) != len(order):
    message = "ordering array contains duplicates"
    raise ValueError(message)
  # at indices selected by the `mask`, sort by `order`. 
  N_sorted = 0
  for f in files:
    r = dragon(f)
    idxs = np.argwhere(eval(mask))
    if len(idxs>0):
      N_sorted += 1
      for ii in idxs:
        r.sort(ii,order)
      r.set_fits()
      r.save_training()
  if N_sorted == 0:
    print("No alterations made.") 
  else:
    print("Number of dragons altered:",N_sorted)


########################################################################
# plotting helper functions 

def ellipse_pars_from_Sigma(Sigma):
  """
  ellipse_pars_from_Sigma(Sigma)
  
  return eigenvalues from 2x2 matrix as well as rotation angle
  for purpose of plotting ellipses
  """
  assert(np.abs(Sigma[0,1]-Sigma[1,0])<1e-13) # make sure it's symmetric
  # determine epicycle parameters
  R = (Sigma[0,0] + Sigma[1,1])/2. # average variance
  r = np.sqrt(.25*(Sigma[0,0]-Sigma[1,1])**2 + Sigma[0,1]*Sigma[1,0])
  lam_plus = R + r # eigenvalue (+): major axis squared
  lam_minus = R - r # eigenvalue (-): minor axis squared
  # determine orientation angle
  if Sigma[1,0]*Sigma[0,1] == 0:
    if Sigma[0,0] < Sigma[1,1]:
      theta = np.pi/2.
    else:
      theta = 0.
  else:
    theta = np.arctan2(lam_plus-Sigma[0,0],Sigma[0,1]) 
  # give parameterization for drawing ellipses
  return lam_plus,lam_minus,theta


def plot_ellipse(Sigma,mu=[0,0],f=1,ax=plt,**kwargs):
  """
  plot_ellipse(Sigma,mu=[0,0],f=1,ax=plt,**kwargs)
  
  plot ellipse given 2x2 matrix, mean mu
  f=1: factor to multiply ellipse width by (for e.g. 2 sigma contours)
  ax=plt: axis to plot on; default = pyplot general
  **kwargs: other keyword arguments for matplotlib.pyplot.plot command
  """
  l1,l2,th = ellipse_pars_from_Sigma(Sigma)
  t = np.arange(0,2*np.pi,.001)
  A,B = f * np.sqrt([l1,l2])[:,np.newaxis] \
          * np.array([np.cos(t),np.sin(t)])
  x = A * np.cos(th) - B * np.sin(th) + mu[0]
  y = A * np.sin(th) + B * np.cos(th) + mu[1]
  ax.plot(x,y,**kwargs)


def plot_all_ellipses(rd,Z,Z_idx=True,colors=[0,1],ax=plt,Nsig=2,
                      color_cycle=mpl_colors_RF,printing=False,**kwargs):
  """
  plot ellipse given full covariance matrix and mean color vector, 
  with information given about which colors to use in plotting
  """
  if Z_idx: # interpret `Z` as an index, rather than a value to sample
    mu = rd._mu[Z]
    Sigma = rd._Sigma[Z]
  else:
    mu = rd.get_mu(Z)
    Sigma = rd.get_Sigma(Z)
  if printing:
    print('Sigma.shape:',Sigma.shape)
    print('mu.shape:',mu.shape)
  for K in range(rd.N_fit):
    sig = Sigma[K][np.ix_(colors,colors)]
    means = mu[K][colors]
    for f in range(1,Nsig+1):
      plot_ellipse(sig,means,f,color=color_cycle[K],**kwargs)


########################################################################
# analysis functions

def to_rd(rd):
  " convert file to red dragon if not already, then return rd object "
  if rd.__class__ == dragon:
    return rd
  elif (rd.__class__ is str) or (rd.__class__ is np.str_):
    return dragon(rd)
  else:
    print("Unrecognized class:",rd.__class__) 


def plot_fits(rd=fname_default,which=[0,1],fit_lines=True,
             color_cycle=mpl_colors_RF):
  """
  plot_fits(rd=fname_default,which=[0,1],fit_lines=True):
  plot fit points and optionally curve fit
  
  rd='output/fit.h5': input filename of parameter fits to plot
  which=[0,1]: array of which of [w,mu,Sigma] to plot; 
              e.g. `1` just plots means
              use [0,1,2] to plot all of [w,mu,Sigma]. 
  fit_lines=True: plot fit lines in addition to parameter data
  color_cycle=mpl_colors_RF: color order for each component
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if rd.__class__ == dragon: # plot exant dragon
    # grab attributes
    Z_bins = rd._Z_bins
    Z_mids = rd._Z_mids
    N_fit = rd.N_fit
    N_cols = rd.N_cols
    # grab points
    w_v = rd._w
    mu_v = rd._mu
    Sigma_v = rd._Sigma
    # grab uncertainties
    w_v_err = rd._w_err
    mu_v_err = rd._mu_err
    Sigma_v_err = rd._Sigma_err
    # grab exclusions and labels
    mask = ~rd._exclusions.astype(bool)
    label_primary = rd.label_primary
    label_colors = rd.label_colors
  else: # read in from file (in case dragon corrupt)
    # grab attributes
    ds = h5py.File(rd,'r')
    Z_bins = ds['Z_bins'][:]
    Z_mids = (Z_bins[1:] + Z_bins[:-1])/2.
    N_fit = ds.attrs['N_fit']
    N_cols = ds.attrs['N_cols']
    if 'exclusions' in ds.attrs.keys():
      mask = ~ds.attrs['exclusions'].astype(bool)
    else:
      mask = np.ones(len(Z_mids)).astype(bool)
    label_primary = "Redshift"
    label_colors = [f"c_{ii}" for ii in range(N_cols)]
    # read in points
    w_v,mu_v,Sigma_v = [ds[key][:] for key in ['w','mu','Sigma']]
    # set up null uncertainties
    def tryload(key):
      if key in ds.keys():
        return ds[key][:]
      else:
        return None
    w_v_err = tryload('w_err')
    mu_v_err = tryload('mu_err')
    Sigma_v_err = tryload('Sigma_err')
    if fit_lines:
      rd = dragon(rd)
  if len(mask[mask]) < 2:
    print("ERROR: too many exclusions; ignoring all exclusions.")
    mask = np.ones_like(mask,dtype=bool)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # grab font size for axes tick sizes
  fs = plt.rcParams['font.size'] # default font size
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if fit_lines:
    zv = np.linspace(Z_bins[0],Z_bins[-1],10**4)
    w = rd.get_weights(zv)
    mu = rd.get_means(zv)
    Sigma = rd.get_covars(zv)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # function for switch
  def check(num):
    " see whether or not to plot given aspect [0,1,2] "
    return (which is num) or (hasattr(which,'__len__') and (num in which))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if check(0): # plot weights
    plt.figure(1)
    e0 = e1 = w_v_err
    for ii in range(N_fit):
      col = color_cycle[ii]
      if w_v_err is not None:
        e0 = w_v_err[~mask,ii]
        e1 = w_v_err[mask,ii]
      plt.errorbar(Z_mids[~mask],w_v[~mask,ii],e0,alpha=.5,
                   fmt='o',color=col,mfc='w',ms=10**.5,lw=1)
      plt.errorbar(Z_mids[mask],w_v[mask,ii],e1,alpha=.5,
                   fmt='o',color=col,ms=10**.5,lw=1)
      if fit_lines:
        plt.plot(zv,w[:,ii],color=col)
    plt.tick_params(labelsize=fs)
    plt.xlabel(label_primary)
    plt.ylabel(r'Weight')
    ylim = plt.ylim()
    plt.ylim(max(0,ylim[0]), min(1,ylim[1]))
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  fgsz = double_width if N_cols > 4 else golden_aspect
  if check(1): # plot means
    fig,axs = plt.subplots(1,N_cols,sharex=True,figsize=fgsz)
    axs[0].set_ylabel('Color')
    e0 = e1 = mu_v_err
    for ii in range(N_cols):
      ax = axs[ii]
      ax.set_xlabel(label_primary)
      lbl = r'$\langle ' + label_colors[ii] + r' \rangle$'
      ax.set_title(lbl,fontsize=fs)
      for jj in range(N_fit):
        col = color_cycle[jj]
        if mu_v_err is not None:
          e0 = mu_v_err[~mask,jj,ii]
          e1 = mu_v_err[mask,jj,ii]
        ax.errorbar(Z_mids[~mask],mu_v[~mask,jj,ii],e0,alpha=.5,
                    fmt='o',color=col,mfc='w',ms=10**.5,lw=1)
        ax.errorbar(Z_mids[mask],mu_v[mask,jj,ii],e1,alpha=.5,
                    fmt='o',color=col,ms=10**.5,lw=1)
        if fit_lines:
          ax.plot(zv,mu[:,jj,ii],color=col)
      if 0: # hardcoded to Bz fit
        ax.set_ylim([.5,.2,.1][ii],None)
      ax.tick_params(labelsize=fs)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if check(2): # plot covariances
    # lbl_temp = r'${\sigma_{%s}}^2$'
    lbl_temp = r'$ %s $'
    fig,axs = plt.subplots(N_cols,N_cols,sharex=True,sharey='row')
    e0 = e1 = Sigma_v_err
    for ii in range(N_cols):
      axs[ii,0].set_ylim(np.nanquantile(Sigma_v[mask,:,:,ii],[0,1]))
      for jj in range(N_cols):
        ax = axs[ii,jj]
        if jj > ii: # upper right triangle
          ax.set_visible(False)
          continue
        if np.all(Sigma_v > 0):
          ax.set_yscale('log')
        if jj == 0: # first column
          ax.set_ylabel(lbl_temp % label_colors[ii])
        if ii == N_cols - 1: # last row
          ax.set_xlabel(label_primary)
        if ii == jj: # main diagonal
          ax.set_title(lbl_temp % label_colors[jj],fontsize=fs)
          # ax.yaxis.set_label_position('right')
          # ax.set_ylabel(lbl_temp % label_colors[ii])
        for kk in range(N_fit):
          col = color_cycle[kk]
          if Sigma_v_err is not None:
            e0,e1 = [Sigma_v_err[M,kk,jj,ii] for M in [~mask,mask]]
            # TODO: convert into proper things for ii==jj and ii!=jj 
          axs[ii,jj].errorbar(Z_mids[~mask],Sigma_v[~mask,kk,jj,ii],
                              e0,alpha=.5,
                              fmt='o',color=col,mfc='w',ms=10**.5,lw=1)
          axs[ii,jj].errorbar(Z_mids[mask],Sigma_v[mask,kk,jj,ii],
                              e1,alpha=.5,
                              fmt='o',color=col,ms=10**.5,lw=1)
          if fit_lines:
            axs[ii,jj].plot(zv,Sigma[:,kk,jj,ii],color=col)
        ax.tick_params(labelsize=fs)
  plt.show()


def plot_scattcorr(rd,fit_lines=True,color_cycle=mpl_colors_RF,
                   plot_color_uncertainties=False):
  """
  decompose covariance matrix into individual scatters and covariances
  
  fit_lines=True: plot red dragon fit of data
  color_cycle=mpl_colors_RF: color order for each component
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in file
  rd = to_rd(rd)
  # grab attributes
  Z_bins = rd._Z_bins
  Z_mids = rd._Z_mids
  N_fit = rd.N_fit
  N_cols = rd.N_cols
  # grab labels
  fs = plt.rcParams['font.size'] # font size
  # grab points
  w_v = rd._w
  mu_v = rd._mu
  Sigma_v = rd._Sigma
  scats = np.sqrt(np.diagonal(Sigma_v,axis1=-1,axis2=-2))
  # grab uncertainties
  w_v_err = rd._w_err
  mu_v_err = rd._mu_err
  Sigma_v_err = rd._Sigma_err
  if Sigma_v_err is not None: # decompose into scatter and correlation
    # scats_err = np.sqrt(np.diagonal(Sigma_v_err,axis1=-1,axis2=-2))
    lg_var,lg_var_err = [np.log(np.diagonal(S,axis1=-1,axis2=-2)) \
                         for S in [Sigma_v,Sigma_v_err]]
    lo = np.sqrt(np.exp(lg_var - lg_var_err))
    hi = np.sqrt(np.exp(lg_var + lg_var_err))
    scats_err = np.array([scats - lo, hi - scats])
    corr_err = _corr_from_Sigma(Sigma_v_err)
  else: # set to null
    scats_err = None
    corr_err = None
  # grab exclusions
  mask = ~rd._exclusions.astype(bool)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # grab fit lines for scatters
  if fit_lines:
    zv = np.linspace(Z_bins[0],Z_bins[-1],10**4)
    w = rd.get_weights(zv)
    mu = rd.get_means(zv)
    Sigma = rd.get_covars(zv)
    sig_vals = np.sqrt(np.diagonal(Sigma,0,-1,-2))
    corr_vals = _corr_from_Sigma(Sigma)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculate and plot (log) scatters
  mn,mx = np.quantile(scats[scats>0],[0,1])
  mn = max(mn/2.,10**np.floor(np.log10(mn)))
  mx = min(mx*2.,10**np.ceil(np.log10(mx)))
  
  fgsz = double_width if N_cols > 4 else golden_aspect
  
  fig,axs = plt.subplots(1,N_cols,sharex=True,sharey=True,num=1,figsize=fgsz)
  for ii in range(N_cols):
    ax = axs[ii]
    if ii==0: # first pass
      ax.set_ylabel('Scatter')
      ax.set_ylim(mn,mx)
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    # plot points
    for kk in range(N_fit):
      col = color_cycle[kk]
      if len(Z_mids[~mask]) > 0:
        ax.scatter(Z_mids[~mask],scats[~mask,kk,ii],10,color=col,facecolor='w')
      mn = scats[mask,kk,ii]
      if scats_err is None:
        em = None
      else:
        em = scats_err[:,mask,kk,ii]
      ax.errorbar(Z_mids[mask],mn,em,fmt='o',color=col,ms=10**.5,alpha=.5)
      ax.set_yscale('log')
      # ax.set_ylabel(r'$\sigma_{%s}$' % rd.label_colors[ii])
      ax.set_title(r'$\sigma_{%s}$' % rd.label_colors[ii],fontsize=fs)
      ax.set_xlabel(rd.label_primary)
      # ax.set_xticklabels(fontsize=fs)
      ax.tick_params(labelsize=fs)
      if fit_lines:
        ax.plot(zv,sig_vals[:,kk,ii],color=col)
      if plot_color_uncertainties:
        # read in color uncertainties from file
        f_in = measure_color_uncertainty(rd.fname_data)
        with h5py.File(f_in,'r') as f:
          keys = ['Z_mids','col_errs']
          zmd,ces = [f[k][:] for k in keys]
        ax.plot(zmd,ces[:,ii,2],color='g',zorder=-10,alpha=.5)
        ax.fill_between(zmd,ces[:,ii,1],ces[:,ii,3],color='g',
                        zorder=-10,alpha=.125,lw=0)
        ax.fill_between(zmd,ces[:,ii,0],ces[:,ii,4],color='g',
                        zorder=-10,alpha=.125,lw=0)
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculate and plot correlations
  corr = _corr_from_Sigma(Sigma_v)
  
  # grab indices to iterate over the correlation matrix
  idx_x,idx_y = np.tril_indices(N_cols,-1)
  
  fig,axs = plt.subplots(1,len(idx_x),sharex=True,sharey=True,num=2,figsize=fgsz)
  for ii in range(len(idx_x)):
    if len(idx_x) > 1:
      ax = axs[ii]
    else: # single correlation; only two colors at play here
      ax = axs
    
    if ii==0: # leftmost plot
      ax.set_ylabel('Correlation')
      # ax.set_ylim(-.21,1.01) # hardcode limits
      ax.set_ylim(-1.01,1.01) # should never be |rho|>1
    for kk in range(N_fit):
      col = color_cycle[kk]
      if len(Z_mids[~mask]) > 0:
        ax.scatter(Z_mids[~mask],corr[~mask,kk,idx_x[ii],idx_y[ii]],
                   10,color=col,facecolor='w')
      if corr_err is None:
        ec = None
      else:
        ec = corr_err[mask,kk,idx_x[ii],idx_y[ii]]
      ax.errorbar(Z_mids[mask],corr[mask,kk,idx_x[ii],idx_y[ii]],
                  ec,fmt='o',ms=10**.5,color=col,alpha=.5)
      lx,ly = [rd.label_colors[v[ii]] for v in [idx_x,idx_y]]
      ax.set_title(r'$\rho(%s, \, %s)$' % (ly,lx),fontsize=fs)
      ax.set_xlabel(rd.label_primary)
      ax.tick_params(labelsize=fs)
      if fit_lines:
        ax.plot(zv,corr_vals[:,kk,idx_x[ii],idx_y[ii]],color=col)
  plt.show()


def plot_correlations(rd,color_cycle=mpl_colors_RF):
  """
  plot lower triangle matrix for inter-color correlations
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in file
  rd = to_rd(rd)
  corr = _corr_from_Sigma(rd._Sigma)
  corr_fit = _corr_from_Sigma(rd.Sigma_fit)
  if rd._Sigma_err is not None:
    corr_err = _corr_from_Sigma(rd._Sigma_err)
  else:
    corr_err = None
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up plot
  # TURN OFF MPL TIGHT LAYOUT
  plt.rcParams['figure.autolayout'] = 'False'
  fs = plt.rcParams['font.size'] # font size
  lbl_y = 'Correlation'
  fgsz = golden_aspect
  if rd.N_cols > 4:
    fgsz = fgsz * 1.5
    lbl_y = r'$\rho$'
  fig,axs = plt.subplots(rd.N_cols-1,rd.N_cols-1,figsize=fgsz,
                         sharex=True,sharey=True,num=1)
  for ii in range(1,rd.N_cols):
    for jj in range(rd.N_cols-1):
      ax = axs[ii-1,jj]
      #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
      # set up axes labels and structure
      if not (ii > jj):
        ax.set_visible(False)
        continue
      if ii == jj + 1: # we're on the diagonal
        ax.set_title('$'+rd.label_colors[jj]+'$',fontsize=fs)
        tw = ax.twinx()
        tw.set_ylabel('$'+rd.label_colors[ii]+'$')
        tw.set_yticks([],minor=False)
        # tw.set_visible(False)
        # ax.yaxis.set_label_position("right")
        # ax.set_ylabel('$'+rd.label_colors[ii]+'$')
      if ii == rd.N_cols-1: # we're on the bottom row
        ax.set_xlabel(rd.label_primary)
      if jj == 0: # we're on the left edge
        ax.set_ylabel(lbl_y)
      lbl = r"$\rho(%s,%s)$" % (rd.label_colors[jj],rd.label_colors[ii])
      #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  
      # plot data
      for kk in range(rd.N_fit):
        col = color_cycle[kk]
        ax.plot(rd.Z_fit,corr_fit[:,kk,ii,jj],color=col)
        ax.errorbar(rd._Z_mids,corr[:,kk,ii,jj],corr_err[:,kk,ii,jj],
                    color=col,alpha=.5,fmt='o',ms=10**.5)
  
  ylim = ax.get_ylim()
  ylim = ax.set_ylim(max(-1,ylim[0]),1)
  # nix middle white space
  plt.subplots_adjust(wspace=0,hspace=0,bottom=.143)
  plt.show()
  # TURN BACK ON MPL TIGHT LAYOUT
  plt.rcParams['figure.autolayout'] = 'True' # turn back on
  return











def plot_gif(rd,data=None,Z_bins=None,dZ=None,coloring=None,display=False,
             colormap=cmap_spec,labels=None,sizing=None,
             xlim=None,ylim=None,factor=.5,
             fnames_out='output/gif%04i.png'):
  """
  plot_gif(rd,data=None,Z_bins=None,dZ=None,coloring=None,display=False,
           colormap=cmap_spec,labels=None,sizing=None,
           xlim=None,ylim=None,factor=.5,fnames_out='output/gif%04i.png')

  factor = amount by which to decrease the size of all points 
           (magnitude dependent at the moment, by i-band.)
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in inputs and convert to proper form
  rd = to_rd(rd) # convert to dragon if not already
  if data is None:
    data = rd.fname_data
  with h5py.File(data,'r') as ds:
    Z,bands = [ds[key][:] for key in ['Z','bands']]
    N = len(Z)
    Z_err = ds['Z_err'][:] if 'Z_err' in ds.keys() else np.zeros(N)
    bands_err = ds['bands_err'][:] if 'bands_err' in ds.keys() else np.zeros(N)
  cols = -np.diff(bands,axis=1)
  # set up redshift limits if not defined
  if Z_bins is None:
    Z_bins = np.quantile(Z,[0,1]) # default = all
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if not hasattr(Z_bins,'__len__'):
    # allow for integer input, i.e. single redshift slice
    if dZ is None:
      dZ = .01 # as default
    Z_bins = [Z_bins - dZ, Z_bins + dZ]
    dZ = None # so it's treated as the bin it is now
  if dZ is None:
    N = len(Z_bins) - 1
  else:
    N = len(Z_bins)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  for ii in range(N):
    if dZ is None:
      Z_lim = Z_bins[ii], Z_bins[ii+1]
    else: # treat Z_bins as Z_mids +/- dZ/.2
      Z_lim = Z_bins[ii] - dZ/2., Z_bins[ii] + dZ/2.
    mask_ii = (Z_lim[0] <= Z) * (Z < Z_lim[1])
    label_ii = f"Z|[{Z_lim[0]:0.3f},{Z_lim[1]:0.3f})"
    Z_med = np.median(Z[mask_ii])
    P_red = rd.P_red(Z_med,bands[mask_ii],bands_err[mask_ii])
    if (coloring is None) or (coloring == 'P_red'):
      # coloring = P_red > .5
      color = 1 - P_red # color by redness (0=red)
    elif coloring == 'pred':
      color = rd.get_pred(Z_med,bands[mask_ii],bands_err[mask_ii])
    # set up sizing of points
    if sizing is None:
      # enlarge bright galaxies; shrink dim ones
      mi_star_pseudo = 21.25 + 2.128 * np.log(Z_med) + 8.30 * Z_med
      mi_star_pseudo = 18.0 # this makes high redshifts cleaner, as there are more galaxies
      mi_star_pseudo = 22.9 + 2.54 * np.log(Z_med) # Bz median i-band
      sizing = mi_star_pseudo - factor - bands[mask_ii,2]
      sizing = np.clip(sizing,0,None) # no negative sizes
    # plot points
    plt.scatter(cols[mask_ii,0],cols[mask_ii,1],sizing,c=color,
                cmap=colormap,alpha=.5)
    plt.title(label_ii)
    if labels is None:
      plt.xlabel(lbl_c % 0)
      plt.ylabel(lbl_c % 1)
    else:
      plt.xlabel(labels[0])
      plt.ylabel(labels[1])
    plt.xlim(xlim)
    plt.ylim(ylim)
    if fnames_out is not None:
      plt.savefig(fnames_out % ii)
      print("Saved",fnames_out % ii)
    if display:
      plt.show()
    else:
      plt.clf()
  return


def plot_WCR(rd):
  """
  plot weight, color relative to scatter, and relative scatter 
  useful to discern which points belong to which element
  """
  rd = to_rd(rd) # convert to dragon if not already
  Z = rd._Z_mids
  W,C,R = rd.get_WCR()
  good = ~(rd._exclusions).astype(bool)
  ######################################################################
  # plot W,C,R independently 
  fig,axs = plt.subplots(3,1,sharex=True,figsize=double_height*np.array([1,.65]))
  for jj in range(3):
    ax = axs[jj]
    y = [W,C,R][jj]
    lbl = [lbl_W,lbl_C,lbl_R][jj]
    ax.set_ylabel(lbl)
    if jj in [0,2] and (np.max(y[good])/np.min(y[good]) > 8):
      ax.set_yscale('log')
    for ii in range(rd.N_fit):
      col = mpl_colors_RF[ii]
      ax.plot(Z[good],y[good,ii],color=col,alpha=.5)
      ax.scatter(Z[:],y[:,ii],5,color=col)
    xlim = ax.set_xlim(ax.get_xlim())
    v = [1./rd.N_fit,0,1][jj]
    ax.plot(xlim,v*ones,'k:',lw=.25)
  axs[-1].set_xlabel('Redshift')
  plt.show()
  ######################################################################
  # plot CR plane
  fig,axs = plt.subplots(2,1,figsize=double_height*np.array([1,.65]))
  ax = axs[0]
  for ii in range(rd.N_fit):
    s = 1 + 50 * W[:,ii]
    col = mpl_colors_RF[ii]
    ax.plot(C[good,ii],R[good,ii],alpha=.5,color=col,lw=1)
    ax.scatter(C[:,ii],R[:,ii],s,color=col)
  xlim = ax.set_xlim(ax.get_xlim())
  ax.plot(xlim,ones,'k:',lw=.25)
  ylim = ax.set_ylim(ax.get_ylim())
  ax.plot(np.zeros(2),ylim,'k:',lw=.25)
  ax.set_xlabel(lbl_C)
  ax.set_ylabel(lbl_R)
  ax.set_yscale('log')
  ######################################################################
  # plot deciding value
  ax = axs[1]
  for ii in range(rd.N_fit):
    col = mpl_colors_RF[ii]
    ax.plot(Z[good],C[good,ii],'-',color=col)
    ax.fill_between(Z[good],(C-.5*R)[good,ii],(C+.5*R)[good,ii],
                     color=col,lw=0,alpha=.125)
    if len(Z[~good]) > 0:
      ax.errorbar(Z[~good],C[~good,ii],.5*R[~good,ii],fmt='o',ms=4,color=col,lw=1)
  xlim = ax.set_xlim(ax.get_xlim())
  ax.plot(xlim,np.zeros(2),'k:',lw=.25)
  ax.set_xlabel('Redshift')
  ax.set_ylabel(lbl_C + ' $\pm$ ' + r'$\frac{1}{2}$' + lbl_R)
  plt.show()


def plot_flat_pars(rd):
  " plot a dragon's flattened pars "
  rd = to_rd(rd) # convert to dragon if not already
  flat = rd.get_flat_pars()
  N,K,F = flat.shape # number of Z bins, components, fit pars
  flat_err = rd.get_flat_pars_err()
  # plot as scatter
  for ii in range(F): # for each parameter
    for jj in range(K): # for each component
      col = mpl_colors_RF[jj]
      xv = ii + rd._Z_mids/rd._Z_bins[-1]
      if flat_err is None:
        plt.scatter(xv,flat[:,jj,ii],10,color=col)
      else:
        plt.errorbar(xv,flat[:,jj,ii],flat_err[:,jj,ii],color=col,fmt='.')
  # set up axes and display
  lbls = rd.get_flat_pars_labels()
  idxs = .5 + np.arange(len(lbls))
  plt.xticks(idxs,lbls,rotation=30)
  plt.show()


def calc_sep(rd):
  " calculate separation between RS & BC "
  rd = to_rd(rd) # convert to dragon if not already
  assert(rd.N_fit==2) # only designed for cf RS to BC
  # calculate relative sigma (how much more scattered is BC than RS?)
  var = np.diagonal(rd._Sigma,0,-1,-2) # grab variances
  sig_RS,sig_BC = [var[:,ii] for ii in range(2)]
  rel = sig_BC/sig_RS
  # calculate delta (d mu / sig_tot)
  d_mu = -np.diff(rd._mu,axis=1).squeeze()
  sig_tot = np.sqrt(np.sum(var,axis=1))
  delta = d_mu/sig_tot
  for lbl,var in zip(['delta','log10 sig_BC/sig_RS'],[delta,np.log10(np.sqrt(rel))]):
    print(f"{lbl}|[{np.nanmin(var):0.3g},{np.nanmax(var):0.3g}]")
    mn,st = [f(var) for f in [np.nanmean,np.nanstd]]
    err = st/np.sqrt(len(var.flatten()))
    print(f"{lbl} = ({mn:0.3g} +/- {err:0.2g}) +/- {st:0.2g}")
    qtls = np.nanquantile(var,[1-s1,.5,s1])
    lo,hi = np.diff(qtls)
    print(f"{lbl} = {qtls[1]:0.3g}^{{+{hi:0.2g}}}_{{-{lo:0.2g}}}"
          f" ~ +/- {np.sqrt(lo*hi):0.2g}")


def plot_number_counts(rd,color_cycle=mpl_colors_RF):
  rd = to_rd(rd)
  # plot all
  N = rd.N_gals
  if N is None:
    N = rd.N_used
  plt.errorbar(rd._Z_mids,N,N**.5,fmt='o',color='k',alpha=.5,ms=10**.5)
  # plot counts in each sub-component
  Ns = (N[:,None] * rd._w).T
  for ii,N in enumerate(Ns):
    col = mpl_colors_RF[ii]
    plt.errorbar(rd._Z_mids,N,N**.5,fmt='o',color=col,alpha=.5,ms=10**.5)
  plt.xlabel(rd.label_primary)
  plt.ylabel(r'$N_{\rm galaxies}$')
  plt.show()


def plot_cZ(rd=None,fname=None,c=None,xlim=None,ylim=None,N=10**5):
  """
  plot_cZ(rd=None,fname=None,c=None,xlim=None,N=10**5)
  
  plot color as a function of redshift
  
  rd=None : red dragon to calculate pred/P_red and/or to sample data from
  fname=None : file to read redshift and magnitude data from
  c=None : which coloring schema to use
    [log redshift error, log color error, magnitude, P_red, pred] 
    if c == 'pred' or -1 : color points by predicted component estimate
    if c == 'P_red' or -2 : color points by probability of K=0
  xlim=None : x-axis plotting limits
  N=10**5 : maximum number of points to grab from data file
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in data
  if (fname is None) and (rd is not None):
    rd = to_rd(rd)
    fname = rd.fname_data
    if c is None:
      c = 'pred'
  elif fname is not None:
    if c is None:
      c = 1
  with h5py.File(fname,'r') as ds:
    Z,bands = [ds[key][:N] for key in ['Z','bands']]
    Z_err = ds['Z_err'][:N] if 'Z_err' in ds.keys() \
                           else np.zeros_like(Z)
    bands_err = ds['bands_err'][:N] if 'bands_err' in ds.keys() \
                                   else None
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up red dragon, if input
  if rd is None:
    pred = np.zeros_like(Z)
    P_red = np.zeros_like(Z)
    cmap = 'bone'
  else:
    rd = to_rd(rd) # convert to dragon if not already
    if c == 'pred':
      c = -1
    elif c == 'P_red':
      c = -2
    pred = rd.get_pred(Z,bands,bands_err)
    P_red = rd.P_red(Z,bands,bands_err)
    cmap = cmap_spec
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # augment data for analysis
  cols = -np.diff(bands,axis=1)
  N_gal,N_cols = np.shape(cols)
  if bands_err is not None:
    col_err = _band_err_to_col_err(bands_err)
  else:
    col_err = np.ones_like(bands)
  for ii in range(N_cols):
    z = [np.log10(Z_err), np.log10(col_err[:,ii]), np.sum(bands,axis=1), P_red, pred][c]
    if c in {0,1,2}:
      mn_z,mx_z = np.quantile(z,[.05,.95])
    else:
      mn_z,mx_z = np.quantile(z,[0,1])
    plt.scatter(Z,cols[:,ii],1,lw=0,alpha=.5,
                c=z,cmap=cmap,vmin=mn_z,vmax=mx_z)
    plt.xlabel('Redshift')
    if xlim is None:
      qtls = 1-s4, s4
      plt.xlim(np.quantile(Z,qtls))
    else:
      plt.xlim(xlim)
    
    if ylim is None:
      qtls = 1-s3, s3
      plt.ylim(np.quantile(cols[:,ii],qtls))
    else:
      plt.ylim(ylim)
    plt.show()


def plot_cf_dragons(dragons,components=[0],colors=[0],covars=[[0,0]],
                    labels=None,scatt=True,add_weights=False,
                    color_cycle=np.tile(np.flip(mpl_colors),10)):
  """
  Compare features between multiple dragons
  
  Inputs
  ------
  dragons
    list of filenames or dragon objects
  components=[0]
    list of components to plot (default = RS)
  colors=[0]: 
    list of colors to plot (default = shortest wavelength)
  covars=[[0,0]]: 
    list of covariance matrix elements to plot
  labels=None: 
    list of labels for each dragon; default = rd.__str__()
  color_cycle=mpl_colors:
    list of colors to use for each dragon
  scatt=True:
    plot scatter instead of variance when available
  add_weights=False:
    if K>2, show dashed lines of two reddest components in weights
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in datasets; create labels
  dragons = [to_rd(rd) for rd in dragons] # convert to dragons if not
  if labels is None:
    # assume component count or dimensionality difference
    labels = [str(rd) for rd in dragons]
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # weights
  for a in components:
    for ii,rd in enumerate(dragons):
      excluded = rd._exclusions.astype(bool)
      col = color_cycle[ii]
      plt.scatter(rd._Z_mids[excluded],rd._w[excluded,a],color=col,
                  facecolor='w')
      plt.scatter(rd._Z_mids[~excluded],rd._w[~excluded,a],color=col)
      Z = np.linspace(rd._Z_bins[0],rd._Z_bins[-1],1000)
      lbl = labels[ii] if a==components[0] else None
      plt.plot(Z,rd.get_weights(Z)[:,a],label=lbl,color=col)
      if (a == 0) and (rd.N_fit > 2) and add_weights:
        # plot dashed lines for summed first two components
        w0 = rd.get_weights(Z)[:,0]
        w1 = rd.get_weights(Z)[:,1]
        plt.plot(Z,w0+w1,'--',color=col)
  plt.xlabel(dragons[0].label_primary)
  plt.ylabel("Weight")
  plt.legend()
  plt.show()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # means
  for b in colors:
    for a in components:
      for ii,rd in enumerate(dragons):
        excluded = rd._exclusions.astype(bool)
        col = color_cycle[ii]
        plt.scatter(rd._Z_mids[excluded],rd._mu[excluded,a,b],color=col,facecolor='w')
        plt.scatter(rd._Z_mids[~excluded],rd._mu[~excluded,a,b],color=col)
        Z = np.linspace(rd._Z_bins[0],rd._Z_bins[-1],1000)
        lbl = labels[ii] if a==components[0] else None
        plt.plot(Z,rd.get_means(Z)[:,a,b],label=lbl,color=col)
    plt.xlabel(dragons[0].label_primary)
    lbl_y = r'$\langle ' + dragons[0].label_colors[b] + r' \rangle$'
    plt.ylabel(lbl_y)
    plt.legend()
    plt.show()
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # covariance
  mn = +np.inf
  for c in covars:
    for a in components:
      for ii,rd in enumerate(dragons):
        Z = np.linspace(rd._Z_bins[0],rd._Z_bins[-1],1000)
        covars_ii = rd.get_covars(Z)[:,a,c[0],c[1]]
        s_ii = rd._Sigma[:,a,c[0],c[1]]
        if scatt and (c[0] == c[1]): # plot scatter instead of variance
          s_ii = np.sqrt(s_ii) 
          covars_ii = np.sqrt(covars_ii)
        excluded = rd._exclusions.astype(bool)
        col = color_cycle[ii]
        if np.min(s_ii[~excluded]) < mn: 
          mn = np.min(s_ii[~excluded])
        plt.scatter(rd._Z_mids[excluded],s_ii[excluded],color=col,facecolor='w')
        plt.scatter(rd._Z_mids[~excluded],s_ii[~excluded],color=col)
        lbl = labels[ii] if a==components[0] else None
        plt.plot(Z,covars_ii,label=lbl,color=col)
    if mn > 0: 
      plt.yscale('log')
      # plt.ylim(mn/1.01,None)
    plt.xlabel(dragons[0].label_primary)
    if scatt and (c[0] == c[1]):
      plt.ylabel(r"$\sigma_{%s}$" % dragons[0].label_colors[c[0]])
    else:
      lbl_c1 = dragons[0].label_colors[c[0]]
      lbl_c2 = dragons[0].label_colors[c[1]]
      plt.ylabel(r"$\sigma_{%s} \cdot \sigma_{%s}$" % (lbl_c1,lbl_c2))
    plt.legend()
    plt.show()


def cf_BIC(rds):
  """
  [deprecated]
  cf_BIC(rds)
  compare BIC values between list of Red Dragons
  currently making very rough estimates on errors
  """
  # grab BIC values
  rds = [to_rd(rd) for rd in rds]
  bics = [rd.BIC for rd in rds]
  Ks = [rd.N_fit for rd in rds]
  lbls = [f"$K={k}$" for k in Ks]
  # prepare x-axis label
  x = rds[0]._Z_mids
  lbl_x = rds[0].label_primary
  # difference in BIC values
  DBIC = bics[1:] - bics[0][np.newaxis,:]
  # simulate errors
  err = np.zeros(np.shape(DBIC))
  e = np.diff(DBIC,axis=1)
  err[:,:-1] = e
  err[:,1:] = np.sqrt(err[:,:-1]**2 + e**2)
  # plot differences in BIC
  for ii in range(DBIC.shape[0]):
    plt.errorbar(x,DBIC[ii],err[ii],ms=2,fmt='o')
  xlim = plt.xlim(plt.xlim())
  plt.plot(xlim,0*np.array(xlim),'k',lw=.25)
  plt.ylabel(r'$\Delta{\rm BIC}$')
  plt.xlabel(lbl_x)
  plt.show()
  # plot significance of that difference
  for ii in range(DBIC.shape[0]):
    plt.scatter(x,DBIC[ii]/err[ii])
  plt.xlabel(lbl_x)
  plt.ylabel('Significance')
  xlim = plt.xlim(plt.xlim())
  plt.plot(xlim,0*np.array(xlim),'k',lw=.25)
  plt.show()
  pass


def plot_cf_BIC(rds):
  """
  plot_cf_BIC(rds)
  compare BIC values between two Red Dragons: `rds` = (rd1,rd2)
  must have identical redshift bins and input data 
  in order for the BIC comparison to be valid
  """
  ######################################################################
  # grab dragons
  rds = [to_rd(rd) for rd in rds]
  ######################################################################
  # check valid input
  # ensure comparing identical redshifts
  assert(np.max(np.abs(rds[0]._Z_mids - rds[1]._Z_mids))<1e-10)
  Z = rds[0]._Z_mids
  lbl_x = rds[0].label_primary
  # ensure using identical datasets
  fnames = [rd.fname_data for rd in rds]
  assert(fnames[0] == fnames[1])
  ######################################################################
  # read in BIC (&al.) values
  bics = np.array([rd.BIC for rd in rds])
  bic_errs = np.array([rd.BIC_err for rd in rds])
  Ks = [rd.N_fit for rd in rds]
  lbls = [f"$K={k}$" for k in Ks]
  # difference in BIC values
  d_BIC = bics[1] - bics[0]
  err_tot = np.sqrt(np.sum(bic_errs**2,axis=0)) if bic_errs[0] is not None else None
  if bic_errs[0] is None:
    err_tot = None
  else:
    err_tot = np.sqrt(bic_errs[0]**2 + bic_errs[1]**2)
  # set up labels
  lbl_dBIC_core = r"({\rm BIC}_{K=%i} - {\rm BIC}_{K=%i})" % (Ks[1],Ks[0])
  lbl_dBIC_sig = "$" + lbl_dBIC_core + r" / \sigma_{\rm BIC, tot}$"
  lbl_dBIC = "$" + lbl_dBIC_core + "/2.$"
  ######################################################################
  # plot BIC values with uncertainties
  for ii in range(2):
    plt.errorbar(Z,bics[ii],bic_errs[ii],fmt='o',label=lbls[ii],ms=10**.5)
  plt.xlabel(lbl_x)
  plt.ylabel("BIC")
  plt.legend()
  plt.show()
  # plot differences in BIC
  err = err_tot/2. if err_tot is not None else None
  plt.errorbar(Z,d_BIC/2.,err,fmt='o',ms=10**.5)
  " Note: divide by two to give ln relative likelihood "
  plt.xlabel(lbl_x)
  xlim = plt.xlim(plt.xlim())
  plt.plot(xlim,[0,0],'k',lw=.5)
  plt.ylabel(lbl_dBIC)
  plt.show()
  if err_tot is None:
    return
  # plot significance of that difference
  plt.scatter(Z,d_BIC/err_tot)
  xlim = plt.xlim(plt.xlim())
  ylim = plt.ylim(plt.ylim())
  for ii in range(1,4):
    plt.fill_between(xlim,-ii*ones,+ii*ones,lw=0,alpha=.125,color='k')
  if True: # plot minimally smoothed middle line
    a,b = Z,d_BIC/err_tot
    while(is_jagged(b)):
      a,b = [mid(c) for c in [a,b]] 
    
    plt.plot(a,b,alpha=.5,color='k')
    pass
  plt.xlabel(lbl_x)
  plt.ylabel(lbl_dBIC_sig)
  plt.show()
  
  ######################################################################
  # plot probabilities of different model superiority?
  if False: # relative likelihoods
    plt.scatter(Z,np.exp(d_BIC/2.))
    yv = [np.exp((d_BIC+mp*err_tot)/2.) for mp in [-1,+1]]
    plt.fill_between(Z,*yv,alpha=.125,lw=0,color='k')
    plt.xlabel(lbl_x)
    plt.ylabel('Relative Likelihood')
    plt.yscale('log')
    plt.show()
  if False: # probability of model 0 being preffered
    plt.scatter(Z,1/(1+np.exp(d_BIC/2.)))
    for ii in range(1,6):
      yv = [np.exp((d_BIC+mp*err_tot)/2.) for mp in [-ii,+ii]]
      plt.fill_between(Z,1/(1+yv[0]),1/(1+yv[1]),alpha=.125,lw=0,color='k')
    plt.xlabel(lbl_x)
    plt.ylabel(f'$P({lbls[0]})$ best')
    plt.ylim(0,1)
    plt.show()


def cf_Pred(rd1,rd2,N=10**4):
  """
  cf_Pred(rd1,rd2,N=10**4)
  compare P_red values for two dragons
  at least compute bACC
  N = number of points to sample
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # read in files and data; calculate P_red
  rd1,rd2 = [to_rd(rd) for rd in [rd1,rd2]]
  # calculate P_red for first dragon
  with h5py.File(rd1.fname_data,'r') as ds:
    Z,Z_err,bands,bands_err = [ds[key][:][:N] for key in keys_data]
    P1 = rd1.P_red(Z,bands,bands_err)
  # calculate P_red for second dragon
  with h5py.File(rd2.fname_data,'r') as ds:
    Z,Z_err,bands,bands_err = [ds[key][:][:N] for key in keys_data]
    P2 = rd2.P_red(Z,bands,bands_err)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if 1: # compute balanced accuracies; call first dragon truth here
    T = np.round(P1).astype(bool)
    P = np.round(P2).astype(bool)
    # confusion matrix elements
    TP = len(T[T*P]) # count of true positives
    FP = len(T[(~T)*P]) # type I error
    FN = len(T[T*(~P)]) # type II error
    TN = len(T[(~T)*(~P)]) # count of true negatives
    # sensitivity and specificity
    TPR = TP/(TP+FN)
    TNR = TN/(TN+FP)
    # balanced accuracy (about equal to accuracy here)
    bACC = (TPR+TNR)/2.
    print(f"bACC: {bACC:0.4g}")
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  dP = P2 - P1
  lbl_dP = r'$P_2 - P_1$'
  if 1: # compute scatter on dP
    sig_tot = np.sqrt(np.mean(dP[~np.isnan(dP)]**2)) # scatter off zero
    sig_low = np.sqrt(np.mean(dP[dP<0]**2))
    sig_high = np.sqrt(np.mean(dP[dP>0]**2))
    print(f"scatter from null: {sig_tot:0.3g}")
    print(f"left scatter: {sig_low:0.3g}")
    print(f"right scatter: {sig_high:0.3g}")
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  if 1: # plot one vs other in 2d histogram (log coloring)
    H,xedges,yedges = np.histogram2d(P1,P2,range=[[0,1],[0,1]],bins=16)
    plt.imshow(np.log10(1+H),origin='lower',extent=(0,1,0,1))
    plt.colorbar(label=r'$\log_{10}(1+N)$')
    plt.xlabel(r'$P_1$')
    plt.ylabel(r'$P_2$')
    plt.show()
  return P1,P2


def plot_slice_fit(rd,indices=None,N=10**3,saving=False,colors=[-2,-1],
                   xlim=None,ylim=None,mag_scaling=False,Nsig=2,
                   fnames_out="slice_fit_%03i.pdf",plot_ellipses=False):
  """
  plot_slice_fit(rd,indices=None,N=10**3,saving=False,colors=[-2,-1],
                 xlim=None,ylim=None,mag_scaling=False,Nsig=2,
                 fnames_out="slice_fit_%03i.pdf",plot_ellipses=False):
  plot slice of RD using default fit (ignoring fitting / exclusions)
  useful in visualizing individual redshit bins
  """
  # read in data
  rd = to_rd(rd) # convert to dragon if not already
  with h5py.File(rd.fname_data,'r') as ds:
    Z,bands = [ds[key][:N] for key in ['Z','bands']]
    Z_err = ds['Z_err'][:N] if 'Z_err' in ds.keys() \
                           else np.zeros_like(Z)
    bands_err = ds['bands_err'][:N] if 'bands_err' in ds.keys() \
                                   else None
  # set up color and noise covariance matrix
  X = -np.diff(bands,axis=1)
  if bands_err is not None:
    X_err = _band_err_to_covar(bands_err)
  else:
    X_err = None
  # grab fit parameters
  Z_bins = rd._Z_bins
  w = rd._w
  mu = rd._mu
  Sigma = rd._Sigma
  # set indices if unset
  if indices is None:
    indices = np.arange(len(Z_bins)-1)
  if not hasattr(indices,'__len__'):
    indices = [indices]
  # start loop over indices
  gmm = pygmmis.GMM(rd.N_fit,rd.N_cols)
  for idx in indices:
    # set up mask
    mn,mx = Z_bins[idx], Z_bins[idx+1]
    mask = (mn <= Z) * (Z < mx)
    lbl_bin = f'{rd.label_primary}|[{mn:0.3g},{mx:0.3g}]'
    plt.title(lbl_bin)
    # set up gmm
    gmm.amp = w[idx]
    gmm.mean = mu[idx]
    gmm.covar = Sigma[idx]
    # get logL values
    logLs = np.array([gmm.logL_k(ii,X[mask],X_err[mask]) for ii in range(gmm.K)])
    pred = np.argmax(logLs,axis=0)
    # plot
    x,y = [X[mask,c] for c in colors]
    lbl_x,lbl_y = [rd.label_colors[c] for c in colors]
    # x = bands[mask,2]; lbl_x = 'i'
    size = 5 # universal point size
    if mag_scaling: # scaled by magnitude
      mag = bands[mask,2] # DES i-band
      def m_str(zed):
        " returns approximation of m_{*,i} "
        return 2.63 * np.log(zed) + np.poly1d([3.6,-2.7,22.0])(zed)
      size = 5 * (m_str(mx) + 2.75 - mag)
    plt.scatter(x,y,size,lw=0,alpha=.5,c=pred,cmap=cmap_RD)
    # plot ellipses for files
    if plot_ellipses:
      for K in range(rd.N_fit):
        sig = gmm.covar[K][np.ix_(colors,colors)]
        means = gmm.mean[K][colors]
        for f in range(1,Nsig+1):
          plot_ellipse(sig,means,f,color=mpl_colors_RF[K])
    # set up axes labels and limits
    qtls = 1-s3, s3
    if xlim is None:
      plt.xlim(np.quantile(x,qtls))
    else:
      plt.xlim(xlim)
    if ylim is None:
      plt.ylim(np.quantile(y,qtls))
    else:
      plt.ylim(ylim)
    plt.xlabel('$'+lbl_x+'$')
    plt.ylabel('$'+lbl_y+'$')
    if saving:
      plt.savefig(fnames_out % idx)
      print("Saved",fnames_out % idx)
    plt.show()


def plot_separation(rd,which=[0,1]):
  """
  plot separation between colors relative to their scatters
  (mu_RS - mu_BC)/sqrt(sig_RS**2 + sig_BC**2)
  
  `which`=[0,1] : determines which to populations are compared
  """
  # convert to red dragon if need be
  rd = to_rd(rd) 
  # set up star metric
  d_mu = np.abs(rd.mu_fit[:,which[0]] - rd.mu_fit[:,which[1]])
  var_vals = [np.diagonal(rd.Sigma_fit[:,idx],0,-1,-2) for idx in which]
  sig_tot = np.sqrt(np.sum(var_vals,axis=0)) # scatters
  star = np.transpose(d_mu/sig_tot) # shape: rd.N_cols, len(Z_vals)
  # plot separation
  for ii in range(rd.N_cols):
    plt.plot(rd.Z_fit,star[ii],label=rd.label_colors[ii])
  plt.legend()
  plt.xlabel(rd.label_primary)
  plt.ylabel('separation')
  xlim = plt.xlim()
  plt.xlim(max(xlim[0],0),rd._Z_bins[-1])
  plt.ylim(0,None)
  plt.show()


def plot_bin_limit(rd,which=0,use_points=False,x=4):
  """
  plot bin width estimator: 
  if you want the RS mean color to vary by less than its width, then 
  we need to have Delta Z < x sig_RS / (d col_RS / dZ), where
  x=4 would be most lenient and x=1 would be most strict 
  """
  # convert to red dragon if need be
  rd = to_rd(rd) 
  # set up which points to use in calculation
  if use_points:
    Z_mids = rd._Z_mids
    mu = rd._mu
    Sigma = rd._Sigma
  else:
    Z_mids = rd.Z_fit
    mu = rd.mu_fit
    Sigma = rd.Sigma_fit
  # calculate midpoints
  Z_midm = (Z_mids[1:] + Z_mids[:-1])/2.
  sig = np.sqrt(np.diagonal(Sigma,0,-1,-2))
  sigm = np.sqrt(sig[1:] * sig[:-1])
  dZ = Z_mids[1] - Z_mids[0] # width of primary bin
  # calculate slope
  slope = np.diff(mu,axis=0) / dZ
  sl_min = min(np.abs(slope[slope!=0]))
  eps = sl_min / 10**6
  dZ_max = x*(sigm/np.abs(slope+eps))[:,which,:]
  print(f"min(dZ_max) = {np.min(dZ_max):0.3g}")
  # plot results
  plt.plot(Z_midm,dZ_max) # max bin width
  plt.xlabel(rd.label_primary)
  plt.ylabel('max bin width')
  plt.yscale('log')
  ylim = plt.ylim()
  mn = min(dZ_max[dZ_max>0])/1.1 # min y-lim
  mx = min(10*(Z_mids[-1] - Z_mids[0]),ylim[1])
  plt.ylim(mn,mx)
  plt.ylabel('max dZ')
  plt.show()


def plot_min_bin_width(rd,color_cycle=mpl_colors_RF):
  """
  for a rd with uncertainty estimates, 
  look at all parameters, aiming for no significant 2nd derivs,
  so that only smooth, linear, gradual trends exist. 
  should ensure that no outlier redshift bins exist, hypothetically.
  """
  rd = to_rd(rd)
  flat = rd.get_flat_pars()
  N,K,F = flat.shape # number of Z bins, components, fit pars
  flat_err = rd.get_flat_pars_err()
  curvature = flat[:-2] - 2 * flat[1:-1] + flat[2:]
  sigma = np.sqrt(flat_err[:-2]**2 + flat_err[1:-1]**2 + flat_err[2:]**2)
  dZ = rd._Z_bins[1] - rd._Z_bins[0]
  dZ_new = dZ * np.sqrt(sigma/np.abs(curvature))
  # plot new bin width
  for ii in range(K):
    col = color_cycle[ii]
    for jj in range(F):
      _=plt.plot(rd._Z_mids[1:-1],dZ_new[:,ii,jj],col,alpha=.5)
    plt.plot(rd._Z_mids[1:-1],np.median(dZ_new[:,ii],axis=1),
             color=col,zorder=5,lw=3,ls=':') # typical value
    plt.plot(rd._Z_mids[1:-1],np.min(dZ_new[:,ii],axis=1),
             color=col,zorder=5,lw=2) # minimum value
  plt.plot(rd._Z_mids[1:-1],np.min(dZ_new,axis=(1,2)),
           color='k',zorder=5,lw=1)
  print(f"typical value: {np.nanmedian(dZ_new):0.3g}")
  print(f"minimum value: {np.nanmin(dZ_new):0.3g}")
  plt.xlabel(rd.label_primary)
  plt.yscale('log')
  plt.ylabel('min bin width')
  plt.show()
  return rd._Z_mids[1:-1], dZ_new


def reconstruct_spectrum(rd,filters='DES',plot_breaks=True):
  """
  from the RD-measured mean colors, reconstruct rest-frame spectrum 
  """
  # ensure input is indeed Red Dragon
  rd = to_rd(rd)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up bandpass middle wavelengths
  if filters == 'DES':
    # DES main filters: g,r,i,z 
    filters = [4730.5 ,  6415.4 ,  7836.21,  9258.37] # Angstrom
  elif filters == 'COSMOS':
    # u,g,r,i,z from DECam + J,H,K from VIRCAM
    filters = [ 3552.98,  4730.5 ,  6415.4 ,  7836.21,  9258.37, 
               12523.9 , 16451.87, 21467.61] # Angstrom
  filters = np.array(filters)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # calculate spectrum from mean colors
  a = 1 / (1 + rd._Z_mids) # scale factor
  lam_rest = filters[None,:] * a[:,None] # shape: (N_bins, N_bands)
  for jj in range(rd.N_fit):
    mags0 = np.zeros((rd.N, rd.N_cols+1))
    for ii in np.flip(range(rd.N_cols)):
      mags0[:,ii] = rd._mu[:,jj,ii] + mags0[:,ii+1]
    mags1 = np.copy(mags0)
    for ii in range(1,rd.N):
      # TODO: consider using more than neighbor to model?
      if True: # use all bands; I think this is a bit better.
        xp,fp = lam_rest[ii-1], mags1[ii-1]
        cs = CubicSpline(xp,fp,bc_type='not-a-knot')
        model = cs(lam_rest[ii])
        shift = np.mean(model - mags1[ii])
      else: # skip leftmost band, as it's extrapolating
        # this works far better than linear interpolation, 
        # and very slightly better than bc_type='not-a-knot'
        xp,fp = lam_rest[ii-1,1:], mags1[ii-1,1:]
        cs = CubicSpline(xp,fp, bc_type='natural')
        model = cs(lam_rest[ii,1:])
        shift = np.mean(model - mags1[ii,1:])
      """
      model = cs(lam_rest[ii,1:])
      # TODO: consider using more than neighbor to model
      shift = np.mean(model - mags1[ii,1:])
      """
      mags1[ii] += shift
    # check mags1 looks good
    #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  #  # 
    # plot output
    for ii in range(rd.N):
      col = mpl_colors_RF[jj] # consider fading w/ age
      flux = 100**(-mags1[ii]/5) / 10**(2*jj)
      plt.scatter(lam_rest[ii],flux,color=col,alpha=a[ii],lw=0)
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up axes
  plt.xscale('log')
  plt.xlabel('Rest-frame Wavelength')
  plt.ylabel('Relative Magnitude')
  plt.yscale('log')
  plt.ylabel('Relative Flux')
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # plot Lyman and Balmer breaks
  if plot_breaks: 
    B = 3645.0682 # Angstroms; Balmer break (Balmer's constant)
    L = 10**10/10973731.568160 # Angstroms; Lyman break (from Rydberg)
    # freeze axes
    xlim = plt.xlim(plt.xlim()) # grab and freeze current xlim
    ylim = plt.ylim(plt.ylim()) # grab and freeze current ylim
    ylim = [ylim[0]/1e3, ylim[1]*1e3] # make line wider than bounds
    # plot lines
    plt.vlines([B,L],*ylim,lw=1,ls='--',color='grey')
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # display
  plt.show()


def colors_by_rest_wavelength(rd,photometry='DES',
                              color_cycle=mpl_colors_RF):
  """
  rescale all colors together to be on rest wavelength rather than Z
  """
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up transition/midpoint wavelengths between bands
  if photometry=='DES': # griz
    lam_tr = [5576.95, 7130.56, 8534.04] # Angstrom
  elif photometry=='COSMOS': # ugriz + JHKs
    lam_tr = [3987.24, 5576.95, 7130.56, 8534.04,
              10840.14, 14190.39, 18915.99] # Angstrom
  else: 
    raise ValueError("ERROR: Photometry not set!")
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # ensure input is indeed Red Dragon
  rd = to_rd(rd)
  for ii in range(rd.N_cols):
    lam = lam_tr[ii] / (1 + rd._Z_mids)
    lam_fit = lam_tr[ii] / (1 + rd.Z_fit)
    for jj in range(rd.N_fit): # plot each component
      plt.errorbar(lam,rd._mu[:,jj,ii],rd._mu_err[:,jj,ii],alpha=.5,
                   fmt='o',color=color_cycle[jj],ms=10**.5,lw=1)
      plt.plot(lam_fit,rd.mu_fit[:,jj,ii],color=color_cycle[jj])
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # set up axes and display 
  plt.xlabel(r'Rest Frame Wavelength ($\AA$)')
  plt.ylabel('Color')
  plt.show()


# Do what is right, no matter the ends. 

